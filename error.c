/*
 * Compile with gcc error.c -lunum -lm -lmpfr -lgmp -o error
 */
#include <stdio.h>
#include <libunum.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

//#define N 100000

double getUboundWidth(ubound_t ub);
double getUboundRelativeError(ubound_t ub);
void testFDP();
void runtests();
double getULPsApart(ubound_t ub);

int main() {

	//testFDP();
	runtests();
	return 0;
}

void runtests() {
	printf("Starting...\n");
	int N = 1000;

	set_env(3, 5);
	_g_fsizemax = 23;
	srand(time(0));
	ubound_t arr[N];
	ubound_t arr2[N];
	ubound_t sum;
	ubound_t mul;
	ubound_t fdp, fdp2;
	ubound_t temp_sum;

	int i;
	double temp;
	double sum_answer = 0, mul_answer = 1, fdp_answer = 0;
	double sum_abs_err[N];
	double sum_rel_err[N];
	double mul_abs_err[N];
	double mul_rel_err[N];
	double fdp_abs_err[N];
	double fdp_rel_err[N];
	double sum_ulp_err[N];
	double mul_ulp_err[N];
	double fdp_ulp_err[N];
	double sum_ulp_stat_err[N];
	double mul_ulp_stat_err[N];

	//initialize
	x2ub(0.0, &sum);
	x2ub(1, &mul);
	x2ub(0, &fdp);
	x2ub(0, &fdp2);

	//generate N floats between (0,1)
	for (i = 0; i < N; ++i) {
		//temp = (double) rand() / (double) RAND_MAX;
		temp = -2.5 + 5 * ((double) rand()) / RAND_MAX;
		x2ub(temp, &arr[i]);

		temp_sum = sum;
		plusubound(&sum, sum, arr[i]);
		timesubound(&mul, mul, arr[i]);
		sum_answer += temp;
		mul_answer *= temp;

		sum_abs_err[i] = getUboundWidth(sum);
		mul_abs_err[i] = getUboundWidth(mul);
		sum_rel_err[i] = getUboundRelativeError(sum);
		mul_rel_err[i] = getUboundRelativeError(mul);
		sum_ulp_err[i] = getULPsApart(sum);
		mul_ulp_err[i] = getULPsApart(mul);

		/*if(sum_ulp_err[i] < sum_ulp_err[i-1]) {
			uboundview(temp_sum);
			uboundview(arr[i]);
			uboundview(sum);
			printf("=====================\n");
		}*/

		//fused dot product
		double op1 = temp;
		ubound_t temp_sum, temp_mul;
		temp = (double) rand() / (double) RAND_MAX;
		x2ub(temp, &arr2[i]);
		fdp_answer += op1 * temp;

		/*ubound_t* temp_arr = malloc((i + 1) * sizeof(ubound_t));
		ubound_t* temp_arr2 = malloc((i + 1) * sizeof(ubound_t));
		memcpy(temp_arr, arr, (i + 1) * sizeof(ubound_t));
		memcpy(temp_arr2, arr2, (i + 1) * sizeof(ubound_t));

		fused_ubound_dot_product(&fdp, (i + 1), temp_arr, temp_arr2);
		free(temp_arr);
		free(temp_arr2);
		*/
		//fused_ubound_dot_product_with_separate_sign_addition(&fdp2, (i + 1), temp_arr, temp_arr2);
		plusubound(&temp_sum, arr[i], arr2[i]);
		timesubound(&temp_mul, arr[i], arr2[i]);

		fdp_abs_err[i] = getUboundWidth(fdp);
		fdp_ulp_err[i] = getULPsApart(fdp);
		sum_ulp_stat_err[i] = getUboundWidth(temp_sum);
		mul_ulp_stat_err[i] = getUboundWidth(temp_mul);
	}

	int fuse_length, l, k;
	for (fuse_length = 2; fuse_length <= N; ++fuse_length) {

		ubound_t temp_a[fuse_length], temp_b[fuse_length], temp_sum;

		for (i = 0; i < N && i + fuse_length < N; i += fuse_length) {
			for (l = 0; l < fuse_length; l++) {
				temp_a[l] = arr[i + l];
				temp_b[l] = arr2[i + l];
			}
			fused_ubound_dot_product(&temp_sum, fuse_length, temp_a, temp_b);
			plusubound(&fdp, fdp, temp_sum);
		}

		l = N % fuse_length;
		if (l > 0) {
			for (k = 0; k < l; k++) {
				temp_a[k] = arr[N - 1 - k];
				temp_b[k] = arr2[N - 1 - k];
			}
			fused_ubound_dot_product(&temp_sum, l, temp_a, temp_b);
			plusubound(&fdp, fdp, temp_sum);
		}

		fdp_abs_err[fuse_length] = getUboundWidth(fdp);
		x2ub(0, &fdp);
	}

	//print arrays to files
	FILE *f;
/*	f = fopen("sum_abs.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", sum_abs_err[i]);
	}
	fclose(f);

	f = fopen("sum_rel.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", sum_rel_err[i]);
	}
	fclose(f);

	f = fopen("mul_abs.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", mul_abs_err[i]);
	}
	fclose(f);

	f = fopen("mul_rel.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", mul_rel_err[i]);
	}
	fclose(f);*/

	f = fopen("fdp_abs.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", fdp_abs_err[i]);
	}
	fclose(f);

/*
	f = fopen("fdp_rel.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", fdp_rel_err[i]);
	}
	fclose(f);

	f = fopen("sum_ulp.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", sum_ulp_err[i]);
	}
	fclose(f);

	f = fopen("mul_ulp.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", mul_ulp_err[i]);
	}
	fclose(f);

	f = fopen("fdp_ulp.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", fdp_ulp_err[i]);
	}
	fclose(f);

	f = fopen("sum_ulp_stat.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", sum_ulp_stat_err[i]);
	}
	fclose(f);

	f = fopen("mul_ulp_stat.txt", "w");
	for (i = 0; i < N; ++i) {
		fprintf(f, "%e, ", mul_ulp_stat_err[i]);
	}
	fclose(f);
*/

	printf("sum double answer = %e\n",sum_answer);
	uboundview(sum);
	printf("mul double answer = %e\n",mul_answer);
	uboundview(mul);

	/*printf("printing array 1\n");
	for (i = 0; i < N; ++i) {
		uboundview(arr[i]);
	}
	printf("printing array 2\n");
	for (i = 0; i < N; ++i) {
		uboundview(arr2[i]);
	}
	printf("answer\n");*/
	printf("fdp double answer = %e\n",fdp_answer);
	uboundview(fdp);
	/*printf("fdp2 double answer = %e\n", fdp_answer);
	uboundview(fdp2);

	int j;
	for (i = 0; i < N; ++i) {
		ubound_t* temp_arr = malloc((i + 1) * sizeof(ubound_t));
		ubound_t* temp_arr2 = malloc((i + 1) * sizeof(ubound_t));
		memcpy(temp_arr, arr, (i + 1) * sizeof(ubound_t));
		memcpy(temp_arr2, arr2, (i + 1) * sizeof(ubound_t));
		fused_ubound_dot_product(&fdp, (i + 1), temp_arr, temp_arr2);
		plusubound(&fdp, arr[i], arr2[i]);
		double ulpwidth = getULPsApart(fdp);
		if(ulpwidth > 2.0) {
			printf("printing vector 1\n");
			for (j = 0; j < i; ++j) {
				uboundview(arr[j]);
			}
			printf("printing vector 2\n");
			for (j = 0; j < i; ++j) {
				uboundview(arr2[j]);
			}
			printf("printing answer %e\n", ulpwidth);
			uboundview(arr[i]);
			uboundview(arr2[i]);
			uboundview(fdp);
			break;
		}
	}*/

	printf("Done!\n");
}

double getUboundRelativeError(ubound_t ub) {
	gbound_t gb;
	double dleft, dright, result;

	get_gbound_from_ubound(&gb, &ub);

	dleft = u2f(gb.left_bound);
	dright = u2f(gb.right_bound);
	result = (dright / dleft);

	return result;
}

double getUboundWidth(ubound_t ub) {
	/*gbound_t gb;
	double dleft, dright, result;

	get_gbound_from_ubound(&gb, &ub);

	dleft = u2f(gb.left_bound);
	dright = u2f(gb.right_bound);
	result = (dright / dleft);

	return result;*/

	gbound_t gb;
	double dleft, dright, result;

	get_gbound_from_ubound(&gb, &ub);

	dleft = u2f(gb.left_bound);
	dright = u2f(gb.right_bound);
	result = (log10(dright / dleft));

	return result;
}

double getULPsApart(ubound_t ub) {
	gbound_t gb;
	int bias_of_op1, bias_of_op2, exponent_of_op1, exponent_of_op2;
	double result;

	get_gbound_from_ubound(&gb, &ub);

	bias_of_op1 = (1 << gb.left_bound.e_size) - 1;
	bias_of_op2 = (1 << gb.right_bound.e_size) - 1;

	if(gb.left_bound.exponent){
		gb.left_bound.fraction = gb.left_bound.fraction | (1ULL << (gb.left_bound.f_size + 1));
		exponent_of_op1 = gb.left_bound.exponent - bias_of_op1;
	} else {
		exponent_of_op1 = 1 - bias_of_op1;
	}

	if (gb.right_bound.exponent) {
		gb.right_bound.fraction = gb.right_bound.fraction | (1ULL << (gb.right_bound.f_size + 1));
		exponent_of_op2 = gb.right_bound.exponent - bias_of_op2;
	} else {
		exponent_of_op2 = 1 - bias_of_op2;
	}

	//max out the fraction
	if (gb.left_bound.f_size < (_g_fsizemax - 1)) {
		gb.left_bound.fraction <<= (_g_fsizemax - 1 - gb.left_bound.f_size);
	}
	if (gb.right_bound.f_size < (_g_fsizemax - 1)) {
		gb.right_bound.fraction <<= (_g_fsizemax - 1 - gb.right_bound.f_size);
	}

	int result_exp = exponent_of_op1 - exponent_of_op2;
	if(result_exp < 0)
		result_exp = -result_exp;

	long long result_frac;
	if(gb.right_bound.fraction < gb.left_bound.fraction)
		result_frac = gb.left_bound.fraction - gb.right_bound.fraction;
	else
		result_frac = gb.right_bound.fraction - gb.left_bound.fraction;

	result = result_frac * pow(2, result_exp);

	return result;

}

void testFDP() {
	ubound_t arr[2];
	ubound_t arr2[2];

	set_env(3, 4);

	arr[0].left_bound.sign = 0;
	arr[0].left_bound.exponent = 3;
	arr[0].left_bound.fraction = 59436;
	arr[0].left_bound.ubit = 1;
	arr[0].left_bound.e_size = 1;
	arr[0].left_bound.f_size = 15;

	arr[0].right_bound.sign = 0;
	arr[0].right_bound.exponent = 3;
	arr[0].right_bound.fraction = 59446;
	arr[0].right_bound.ubit = 1;
	arr[0].right_bound.e_size = 1;
	arr[0].right_bound.f_size = 15;

	arr[1].left_bound.sign = 0;
	arr[1].left_bound.exponent = 6;
	arr[1].left_bound.fraction = 246;
	arr[1].left_bound.ubit = 1;
	arr[1].left_bound.e_size = 2;
	arr[1].left_bound.f_size = 15;

	arr[1].right_bound.sign = 0;
	arr[1].right_bound.exponent = 6;
	arr[1].right_bound.fraction = 252;
	arr[1].right_bound.ubit = 1;
	arr[1].right_bound.e_size = 2;
	arr[1].right_bound.f_size = 15;

	arr2[0].left_bound.sign = 0;
	arr2[0].left_bound.exponent = 2;
	arr2[0].left_bound.fraction = 10537;
	arr2[0].left_bound.ubit = 1;
	arr2[0].left_bound.e_size = 2;
	arr2[0].left_bound.f_size = 15;

	arr2[0].right_bound.sign = 0;
	arr2[0].right_bound.exponent = 2;
	arr2[0].right_bound.fraction = 10537;
	arr2[0].right_bound.ubit = 1;
	arr2[0].right_bound.e_size = 2;
	arr2[0].right_bound.f_size = 15;

	arr2[1].left_bound.sign = 0;
	arr2[1].left_bound.exponent = 2;
	arr2[1].left_bound.fraction = 46455;
	arr2[1].left_bound.ubit = 1;
	arr2[1].left_bound.e_size = 2;
	arr2[1].left_bound.f_size = 15;

	arr2[1].right_bound.sign = 0;
	arr2[1].right_bound.exponent = 2;
	arr2[1].right_bound.fraction = 46455;
	arr2[1].right_bound.ubit = 1;
	arr2[1].right_bound.e_size = 2;
	arr2[1].right_bound.f_size = 15;

	/*int i;
	ubound_t fdp;

	printf("printing array 1\n");
	for (i = 0; i < 2; ++i) {
		uboundview(arr[i]);
	}
	printf("printing array 2\n");
	for (i = 0; i < 2; ++i) {
		uboundview(arr2[i]);
	}
	printf("answer\n");
	fused_ubound_dot_product(&fdp, 2, arr, arr2);
	uboundview(fdp);*/

	int op1ulp = getULPsApart(arr[0]);
	int op2ulp = getULPsApart(arr[1]);

	uboundview(arr[0]);
	uboundview(arr[1]);

	printf("op1 ulps = %llu, op2 ulps = %llu\n",op1ulp, op2ulp);

}

