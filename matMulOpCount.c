#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#define MAT_SIZE 4
#define REPEAT 1
#define BLOCK MAT_SIZE/4
#define IS_POWER_OF_2(n) ((n != 0) && ((n & (n - 1)) == 0))
#define OP_COUNT
#define FUSE_LENGTH 1
#define PERMNUMBER 4	//number of different permutations
#define AVG
#define ADDCOUNT
#define REGULAR_ADDS

int permuatations[8][12] = { {1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4},
		{3, 4, 1, 2, 1, 2, 3, 4, 3, 4, 1, 2},
		{1, 2, 3, 4, 2, 1, 4, 3, 2, 1, 4, 3},
		{4, 3, 2, 1, 4, 3, 2, 1, 4, 3, 2, 1} };
int currentPermutationIndex = 0;

int mul_count = 0;
int add_count = 0;

typedef struct operations {
	int adds;
	int muls;
	int fused3add;
	int fused4add;
	int fusedDP;
} ops;

void doMatMul(int mat_size);
void assignRandomFloatMatrix(float** matrix, int rows, int columns);
void matMulTraditionalFloat(int n, ops** A, ops** B, ops** C);
void matMulTraditionalFusedU(int n, ops** A, ops** B, ops** C);
float** createFloatArray(int rows, int columns);
void matrixSub(ops** A, ops** B, ops** C, int size);
void matMulStrassenBlocking(int n, ops** A, ops** B, ops** C);
void matMulStrassenBlockingFused(int n, ops** A, ops** B, ops** C);
void matMulStrassenBlockingAllFused(int n, ops** A, ops** B, ops** C);
void matrixAdd(ops** A, ops** B, ops** C, int size);
void combineMatrices(ops** C11, ops** C12, ops** C21, ops** C22, int n,
		ops** result);
void matMulStrassenBlockingRotated(int n, ops** A, ops** B, ops** C);
void matMulStrassenBlockingAllFusedRotated(int n, ops** A, ops** B, ops** C);
void reset_counts();
void printMatrix(size_t n, float** mat);
void printOpsMatrix(size_t n, ops** mat);
void printOpsMatrixCounts(size_t n, ops** mat);
ops** createOpsArray(int rows, int columns);
void destroyArray(ops** arr);
void matrixAddWithTwoFusedOps(ops** A, ops** B, ops** C, ops** result, int size);
void negateUboundMatrix(ops** negated, ops** mat, int size);
void matrixAddWithThreeFusedOps(ops** A, ops** B, ops** C, ops** D, ops** result, int size);
void printToFile(ops** matrix, char* file_name);
void matMulStrassenBlockingAllFusedRotatedCustom(int n, ops** A, ops** B, ops** C, char* mat_name);
void copyMatrix(int size, ops** mat1, ops** mat2);
double getStdDevTotalOpCount(int size, ops** mat);
void clearMatrix(int size, ops** mat);
double getAvgOpCount(int size, ops** mat);
int compare( const void* a, const void* b);
int get90percentile(int size, ops** mat);
double getStdDevAddCount(int size, ops** mat);
int get99percentileAddCount(int size, ops** mat);
int getMinCount(int size, ops** mat);
int get99percentileAddMulCount(int size, ops** mat);
int breaktie(int size, ops** C, ops** Ctemp);

int main() {
	doMatMul(MAT_SIZE);
}

void doMatMul(int mat_size) {

	ops **f1, **f2, **fresult, **fStrassenOneLevel, **fStrassenOneLevelFused, **fStrassenAllFused,
			**fStrassenBlockingAllFusedRotated, **fStrassenAllFusedCustomRotated;

	int i, j;
	unsigned long long effective_total = 0;
	unsigned long long max = 0, min = 0xffffffffffffffff, temp = 0;
	char str[100];

	f1 = createOpsArray(mat_size, mat_size);
	f2 = createOpsArray(mat_size, mat_size);
	fresult = createOpsArray(mat_size, mat_size);
	fStrassenOneLevel = createOpsArray(mat_size, mat_size);
	fStrassenOneLevelFused = createOpsArray(mat_size, mat_size);
	fStrassenBlockingAllFusedRotated = createOpsArray(mat_size, mat_size);
	fStrassenAllFused = createOpsArray(mat_size, mat_size);
	fStrassenAllFusedCustomRotated = createOpsArray(mat_size, mat_size);

	printf("n = %d\nBlock = %d\n", MAT_SIZE, BLOCK);


	/*	matMulTraditionalFloat(mat_size, f1, f2, fresult);
#ifdef OP_COUNT
	printf("tradition add count: %d, mul count: %d, total = %llu", add_count,
			mul_count, add_count + mul_count);
	for (i = 0; i < mat_size; ++i) {
		for (j = 0; j < mat_size; ++j) {
			temp = fresult[i][j].adds + fresult[i][j].muls;
			effective_total += temp;
			if (temp > max)
				max = temp;
			if (temp < min)
				min = temp;
		}
	}
	printf(", effective total = %llu max = %llu, min = %llu \n", effective_total, max, min);
	sprintf(str, "Traditoinal_%d", MAT_SIZE);
	//printToFile(fresult, str);
	printOpsMatrixCounts(MAT_SIZE, fresult);
	reset_counts();
#endif

		matMulStrassenBlocking(mat_size, f1, f2, fStrassenOneLevel);
#ifdef OP_COUNT
	effective_total = 0;
	printf("Strassen block add count: %d, mul count: %d, total = %d", add_count,
			mul_count, add_count + mul_count);
	for (i = 0; i < mat_size; ++i) {
		for (j = 0; j < mat_size; ++j) {
			effective_total += fStrassenOneLevel[i][j].adds
					+ fStrassenOneLevel[i][j].muls;
		}
	}
	printf(", effective total = %llu\n", effective_total);
	sprintf(str, "Strassen_%d", MAT_SIZE);
	//printToFile(fStrassenOneLevel, str);
	printOpsMatrixCounts(MAT_SIZE, fStrassenOneLevel);
	reset_counts();
#endif

		matMulStrassenBlockingRotated(mat_size, f1, f2, fStrassenBlockingRotated);
#ifdef OP_COUNT
	effective_total = 0;
	printf("Strassen rotated add count: %d, mul count: %d, total = %d",
			add_count, mul_count, add_count + mul_count);
	for (i = 0; i < mat_size; ++i) {
		for (j = 0; j < mat_size; ++j) {
			effective_total += fStrassenBlockingRotated[i][j].adds
					+ fStrassenBlockingRotated[i][j].muls;
		}
	}
	printf(", effective total = %llu\n", effective_total);
	reset_counts();
#endif
*/

	/*matMulStrassenBlockingFused(mat_size, f1, f2, fStrassenOneLevelFused);
#ifdef OP_COUNT
	effective_total = 0;
	printf("Strassen block fused add count: %d, mul count: %d, total = %llu",
			add_count, mul_count, add_count + mul_count);
	for (i = 0; i < mat_size; ++i) {
		for (j = 0; j < mat_size; ++j) {
			effective_total += fStrassenOneLevelFused[i][j].adds
					+ fStrassenOneLevelFused[i][j].muls;
		}
	}
	printf(", effective total = %llu\n", effective_total);
	reset_counts();
	printOpsMatrixCounts(MAT_SIZE, fStrassenOneLevelFused);
#endif

	matMulStrassenBlockingAllFused(mat_size, f1, f2, fStrassenAllFused);
#ifdef OP_COUNT
	effective_total = 0;
	max = 0; min = 0xffffffffffffffff;
	for (i = 0; i < mat_size; ++i) {
		for (j = 0; j < mat_size; ++j) {
			temp = fStrassenAllFused[i][j].adds + fStrassenAllFused[i][j].muls;
			effective_total += temp;
			if(temp > max)
				max = temp;
			if(temp < min)
				min = temp;
		}
	}
	printf("Strassen all fused add count: %d, mul count: %d, total = %d", add_count, mul_count, add_count + mul_count);
	printf(" effective total = %llu max = %llu, min = %llu \n", effective_total, max, min);
	sprintf(str, "Strassen_fused_%d", MAT_SIZE);
	//printToFile(fStrassenAllFused, str);
	printOpsMatrixCounts(MAT_SIZE, fStrassenAllFused);
	reset_counts();
#endif

		currentPermutationIndex = 0;
	matMulStrassenBlockingAllFusedRotated(mat_size, f1, f2, fStrassenBlockingAllFusedRotated);
#ifdef OP_COUNT
	effective_total = 0;
	printf("Strassen block all fused rotated add count: %d, mul count: %d, total = %llu",
			add_count, mul_count, add_count + mul_count);
	for (i = 0; i < mat_size; ++i) {
		for (j = 0; j < mat_size; ++j) {
			effective_total += fStrassenBlockingAllFusedRotated[i][j].adds
					+ fStrassenBlockingAllFusedRotated[i][j].muls;
		}
	}
	printf(", effective total = %llu\n", effective_total);
	reset_counts();
	printOpsMatrix(MAT_SIZE, fStrassenBlockingAllFusedRotated);
#endif

*/
	matMulStrassenBlockingAllFusedRotatedCustom(mat_size, f1, f2, fStrassenAllFusedCustomRotated, "initial");
}

void assignRandomFloatMatrix(float** matrix, int rows, int columns) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; j++) {
			matrix[i][j] = -1 + 2 * ((float) rand()) / RAND_MAX;
		}
	}
	return;
}

void matMulTraditionalFloat(int n, ops** A, ops** B, ops** C) {
	int i, j, k, maxaddA = 0, maxaddB = 0, maxmul = 0;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				//find the maximum additions and multiplications of the row and column
				if (maxaddA < A[i][k].adds) {
					maxaddA = A[i][k].adds;
				}

				if (maxaddB < B[k][j].adds) {
					maxaddB = B[k][j].adds;
				}

				if (maxmul < A[i][k].muls) {
					maxmul = A[i][k].muls;
				}

				if (maxmul < B[k][j].muls) {
					maxmul = B[k][j].muls;
				}
			}
			C[i][j].adds = (n -1) + maxaddA + maxaddB;
			C[i][j].muls = 1 + maxmul;
			maxaddA = 0;
			maxaddB = 0;
			maxmul = 0;
		}
	}
	return;

	/*
	 for (i = 0; i < n; i++) {
	 for (j = 0; j < n; j++) {
	 C[i][j].adds = A[i][j].adds + B[i][j].adds;
	 C[i][j].muls = A[i][j].muls + B[i][j].muls;
	 }
	 }

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				//C[i][j] += A[i][k] * B[k][j];
				C[i][j].adds += (A[i][k].adds + B[k][j].adds);
				C[i][j].muls += (A[i][k].muls + B[k][j].muls);

#ifdef OP_COUNT
				if (k == 0) {
					mul_count++;
					C[i][j].muls++;
				} else {
					add_count++;
					mul_count++;
					C[i][j].adds++;
					C[i][j].muls++;
				}
#endif
			}
		}
	}*/
}

void matMulTraditionalFusedU(int n, ops** A, ops** B, ops** C) {
	int i, j, k, maxaddA = 0, maxaddB = 0, maxmul = 0;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				//find the maximum additions and multiplications of the row and column
				if (maxaddA < A[i][k].adds) {
					maxaddA = A[i][k].adds;
				}

				if (maxaddB < B[k][j].adds) {
					maxaddB = B[k][j].adds;
				}

				if (maxmul < A[i][k].fusedDP) {
					maxmul = A[i][k].fusedDP;
				}

				if (maxmul < B[k][j].fusedDP) {
					maxmul = B[k][j].fusedDP;
				}
			}
			C[i][j].adds = 1 + maxaddA + maxaddB;
			C[i][j].fusedDP = 1 + maxmul;
			maxaddA = 0;
			maxaddB = 0;
			maxmul = 0;
		}
	}
	printOpsMatrixCounts(n, A);
	printOpsMatrixCounts(n, B);
	printf("=======================================\n");
	printOpsMatrixCounts(n, C);
	printf("=======================================\n\n");
	return;

	/*int i, j, k;

	 for (i = 0; i < n; i++) {
	 for (j = 0; j < n; j++) {
	 for (k = 0; k < n; k++) {
	 C[i][j].adds += (A[i][k].adds + B[k][j].adds);
	 C[i][j].muls += (A[i][k].muls + B[k][j].muls);
	 }
	 C[i][j].fusedDP++;
	 mul_count++;
	 add_count++;
	 }
	 }

	 for (i = 0; i < n; i++) {
	 for (j = 0; j < n; j++) {
	 C[i][j].adds = (A[i][j].adds + B[i][j].adds);
	 C[i][j].muls = (A[i][j].muls + B[i][j].muls);
	 C[i][j].adds++;
	 C[i][j].muls++;
	 C[i][j].fusedDP++;
	 mul_count++;
	 add_count++;
	 }
	 }*/
}

float** createFloatArray(int rows, int columns) {
	float* values = calloc(rows * columns, sizeof(float));
	float** valueRows = malloc(columns * sizeof(float*));
	for (int i = 0; i < columns; ++i) {
		valueRows[i] = values + i * rows;
	}
	return valueRows;
}

ops** createOpsArray(int rows, int columns) {
	ops* values = calloc(rows * columns, sizeof(ops));
	ops** valueRows = malloc(columns * sizeof(ops*));
	for (int i = 0; i < columns; ++i) {
		valueRows[i] = values + i * rows;
	}
	return valueRows;
}

void matMulStrassenBlocking(int n, ops** A, ops** B, ops** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFloat(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ops** A11 = createOpsArray(size, size);
	ops** A12 = createOpsArray(size, size);
	ops** A21 = createOpsArray(size, size);
	ops** A22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j].adds = A[i][j].adds;
			A11[i][j].muls = A[i][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j].adds = A[i][j + size].adds;
			A12[i][j].muls = A[i][j + size].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j].adds = A[i + size][j].adds;
			A21[i][j].muls = A[i + size][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j].adds = A[i + size][j + size].adds;
			A22[i][j].muls = A[i + size][j + size].muls;
		}
	}

	ops** B11 = createOpsArray(size, size);
	ops** B12 = createOpsArray(size, size);
	ops** B21 = createOpsArray(size, size);
	ops** B22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j].adds = B[i][j].adds;
			B11[i][j].muls = B[i][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j].adds = B[i][j + size].adds;
			B12[i][j].muls = B[i][j + size].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j].adds = B[i + size][j].adds;
			B21[i][j].muls = B[i + size][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j].adds = B[i + size][j + size].adds;
			B22[i][j].muls = B[i + size][j + size].muls;
		}
	}

	ops** S1 = createOpsArray(size, size);
	ops** S2 = createOpsArray(size, size);
	ops** S3 = createOpsArray(size, size);
	ops** S4 = createOpsArray(size, size);
	ops** S5 = createOpsArray(size, size);
	ops** S6 = createOpsArray(size, size);
	ops** S7 = createOpsArray(size, size);
	ops** S8 = createOpsArray(size, size);
	ops** M1 = createOpsArray(size, size);
	ops** M2 = createOpsArray(size, size);
	ops** M3 = createOpsArray(size, size);
	ops** M4 = createOpsArray(size, size);
	ops** M5 = createOpsArray(size, size);
	ops** M6 = createOpsArray(size, size);
	ops** M7 = createOpsArray(size, size);
	ops** T2 = createOpsArray(size, size);
	ops** T3 = createOpsArray(size, size);
	ops** T6 = createOpsArray(size, size);
	ops** C11 = createOpsArray(size, size);
	ops** C12 = createOpsArray(size, size);
	ops** C21 = createOpsArray(size, size);
	ops** C22 = createOpsArray(size, size);

	matrixAdd(A21, A22, S1, size);
	matrixSub(S1, A11, S2, size);
	matrixSub(A11, A21, S3, size);
	matrixSub(A12, S2, S4, size);
	matrixSub(B12, B11, S5, size);
	matrixSub(B22, S5, S6, size);
	matrixSub(B22, B12, S7, size);
	matrixSub(B21, S6, S8, size);
	matMulStrassenBlocking(size, A11, B11, M1);
	matMulStrassenBlocking(size, A12, B21, M2);
	matMulStrassenBlocking(size, S1, S5, M3);
	matMulStrassenBlocking(size, S2, S6, M4);
	matMulStrassenBlocking(size, S3, S7, M5);
	matMulStrassenBlocking(size, S4, B22, M6);
	matMulStrassenBlocking(size, A22, S8, M7);
	matrixAdd(M1, M2, C11, size);
	matrixAdd(M1, M4, T2, size);
	matrixAdd(T2, M5, T3, size);
	matrixAdd(T3, M7, C21, size);
	matrixAdd(T3, M3, C22, size);
	matrixAdd(T2, M3, T6, size);
	matrixAdd(T6, M6, C12, size);
	combineMatrices(C11, C12, C21, C22, n, C);

	destroyArray(A11);
	destroyArray(A12);
	destroyArray(A21);
	destroyArray(A22);
	destroyArray(B11);
	destroyArray(B12);
	destroyArray(B21);
	destroyArray(B22);
	destroyArray(C11);
	destroyArray(C12);
	destroyArray(C21);
	destroyArray(C22);
	destroyArray(S1);
	destroyArray(S2);
	destroyArray(S3);
	destroyArray(S4);
	destroyArray(S5);
	destroyArray(S6);
	destroyArray(S7);
	destroyArray(S8);
	destroyArray(M1);
	destroyArray(M2);
	destroyArray(M3);
	destroyArray(M4);
	destroyArray(M5);
	destroyArray(M6);
	destroyArray(M7);
	destroyArray(T2);
	destroyArray(T3);
	destroyArray(T6);
}

void matMulStrassenBlockingFused(int n, ops** A, ops** B, ops** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ops** A11 = createOpsArray(size, size);
	ops** A12 = createOpsArray(size, size);
	ops** A21 = createOpsArray(size, size);
	ops** A22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j].adds = A[i][j].adds;
			A11[i][j].muls = A[i][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j].adds = A[i][j + size].adds;
			A12[i][j].muls = A[i][j + size].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j].adds = A[i + size][j].adds;
			A21[i][j].muls = A[i + size][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j].adds = A[i + size][j + size].adds;
			A22[i][j].muls = A[i + size][j + size].muls;
		}
	}

	ops** B11 = createOpsArray(size, size);
	ops** B12 = createOpsArray(size, size);
	ops** B21 = createOpsArray(size, size);
	ops** B22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j].adds = B[i][j].adds;
			B11[i][j].muls = B[i][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j].adds = B[i][j + size].adds;
			B12[i][j].muls = B[i][j + size].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j].adds = B[i + size][j].adds;
			B21[i][j].muls = B[i + size][j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j].adds = B[i + size][j + size].adds;
			B22[i][j].muls = B[i + size][j + size].muls;
		}
	}

	ops** S1 = createOpsArray(size, size);
	ops** S2 = createOpsArray(size, size);
	ops** S3 = createOpsArray(size, size);
	ops** S4 = createOpsArray(size, size);
	ops** S5 = createOpsArray(size, size);
	ops** S6 = createOpsArray(size, size);
	ops** S7 = createOpsArray(size, size);
	ops** S8 = createOpsArray(size, size);
	ops** M1 = createOpsArray(size, size);
	ops** M2 = createOpsArray(size, size);
	ops** M3 = createOpsArray(size, size);
	ops** M4 = createOpsArray(size, size);
	ops** M5 = createOpsArray(size, size);
	ops** M6 = createOpsArray(size, size);
	ops** M7 = createOpsArray(size, size);
	ops** T2 = createOpsArray(size, size);
	ops** T3 = createOpsArray(size, size);
	ops** T6 = createOpsArray(size, size);
	ops** C11 = createOpsArray(size, size);
	ops** C12 = createOpsArray(size, size);
	ops** C21 = createOpsArray(size, size);
	ops** C22 = createOpsArray(size, size);

	matrixAdd(A21, A22, S1, size);
	matrixSub(S1, A11, S2, size);
	matrixSub(A11, A21, S3, size);
	matrixSub(A12, S2, S4, size);
	matrixSub(B12, B11, S5, size);
	matrixSub(B22, S5, S6, size);
	matrixSub(B22, B12, S7, size);
	matrixSub(B21, S6, S8, size);

	matMulStrassenBlockingFused(size, A11, B11, M1);
	matMulStrassenBlockingFused(size, A12, B21, M2);
	matMulStrassenBlockingFused(size, S1, S5, M3);
	matMulStrassenBlockingFused(size, S2, S6, M4);
	matMulStrassenBlockingFused(size, S3, S7, M5);
	matMulStrassenBlockingFused(size, S4, B22, M6);
	matMulStrassenBlockingFused(size, A22, S8, M7);

	matrixAdd(M1, M2, C11, size);
	matrixAdd(M1, M4, T2, size);
	matrixAdd(T2, M5, T3, size);
	matrixAdd(T3, M7, C21, size);
	matrixAdd(T3, M3, C22, size);
	matrixAdd(T2, M3, T6, size);
	matrixAdd(T6, M6, C12, size);
	combineMatrices(C11, C12, C21, C22, n, C);

	destroyArray(A11);
	destroyArray(A12);
	destroyArray(A21);
	destroyArray(A22);
	destroyArray(B11);
	destroyArray(B12);
	destroyArray(B21);
	destroyArray(B22);
	destroyArray(C11);
	destroyArray(C12);
	destroyArray(C21);
	destroyArray(C22);
	destroyArray(S1);
	destroyArray(S2);
	destroyArray(S3);
	destroyArray(S4);
	destroyArray(S5);
	destroyArray(S6);
	destroyArray(S7);
	destroyArray(S8);
	destroyArray(M1);
	destroyArray(M2);
	destroyArray(M3);
	destroyArray(M4);
	destroyArray(M5);
	destroyArray(M6);
	destroyArray(M7);
	destroyArray(T2);
	destroyArray(T3);
	destroyArray(T6);
}

void matMulStrassenBlockingAllFusedRotatedCustom(int n, ops** A, ops** B,
		ops** C, char* mat_name) {
	int i, j;
	int thisPermutation[12];
	int indexi[4];
	int indexj[4];
	double minSD = INFINITY, SD = 0, minMaxAvg = INFINITY, max = INFINITY, min = 0;
	char buf[100];

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		//matMulTraditionalFloat(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ops** A11 = createOpsArray(size, size);
	ops** A12 = createOpsArray(size, size);
	ops** A21 = createOpsArray(size, size);
	ops** A22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j].adds = A[i][j].adds;
			A11[i][j].muls = A[i][j].muls;
			A11[i][j].fusedDP = A[i][j].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j].adds = A[i][j + size].adds;
			A12[i][j].muls = A[i][j + size].muls;
			A12[i][j].fusedDP = A[i][j + size].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j].adds = A[i + size][j].adds;
			A21[i][j].muls = A[i + size][j].muls;
			A21[i][j].fusedDP = A[i + size][j].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j].adds = A[i + size][j + size].adds;
			A22[i][j].muls = A[i + size][j + size].muls;
			A22[i][j].fusedDP = A[i + size][j + size].fusedDP;
		}
	}

	ops** B11 = createOpsArray(size, size);
	ops** B12 = createOpsArray(size, size);
	ops** B21 = createOpsArray(size, size);
	ops** B22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j].adds = B[i][j].adds;
			B11[i][j].muls = B[i][j].muls;
			B11[i][j].fusedDP = B[i][j].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j].adds = B[i][j + size].adds;
			B12[i][j].muls = B[i][j + size].muls;
			B12[i][j].fusedDP = B[i][j + size].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j].adds = B[i + size][j].adds;
			B21[i][j].muls = B[i + size][j].muls;
			B21[i][j].fusedDP = B[i + size][j].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j].adds = B[i + size][j + size].adds;
			B22[i][j].muls = B[i + size][j + size].muls;
			B22[i][j].fusedDP = B[i + size][j + size].fusedDP;
		}
	}

	ops** S1 = createOpsArray(size, size);
	ops** S2 = createOpsArray(size, size);
	ops** S3 = createOpsArray(size, size);
	ops** S4 = createOpsArray(size, size);
	ops** S5 = createOpsArray(size, size);
	ops** S6 = createOpsArray(size, size);
	ops** S7 = createOpsArray(size, size);
	ops** S8 = createOpsArray(size, size);
	ops** M1 = createOpsArray(size, size);
	ops** M2 = createOpsArray(size, size);
	ops** M3 = createOpsArray(size, size);
	ops** M4 = createOpsArray(size, size);
	ops** M5 = createOpsArray(size, size);
	ops** M6 = createOpsArray(size, size);
	ops** M7 = createOpsArray(size, size);
	ops** T1 = createOpsArray(size, size);
	ops** T2 = createOpsArray(size, size);
	ops** T3 = createOpsArray(size, size);
	ops** C11 = createOpsArray(size, size);
	ops** C12 = createOpsArray(size, size);
	ops** C21 = createOpsArray(size, size);
	ops** C22 = createOpsArray(size, size);
	ops** temp = createOpsArray(size, size);
	ops** temp2 = createOpsArray(size, size);

	matrixAdd(A21, A22, S1, size);
	matrixSub(S1, A11, S2, size);
	matrixSub(A11, A21, S3, size);
	matrixSub(A12, S2, S4, size);
	matrixSub(B12, B11, S5, size);
	matrixSub(B22, S5, S6, size);
	matrixSub(B22, B12, S7, size);
	matrixSub(B21, S6, S8, size);

	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M1");
	matMulStrassenBlockingAllFusedRotatedCustom(size, A11, B11, M1, buf);
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M2");
	matMulStrassenBlockingAllFusedRotatedCustom(size, A12, B21, M2, buf);
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M3");
	matMulStrassenBlockingAllFusedRotatedCustom(size, S1, S5, M3, buf);
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M4");
	matMulStrassenBlockingAllFusedRotatedCustom(size, S2, S6, M4, buf);
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M5");
	matMulStrassenBlockingAllFusedRotatedCustom(size, S3, S7, M5, buf);
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M6");
	matMulStrassenBlockingAllFusedRotatedCustom(size, S4, B22, M6, buf);
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M7");
	matMulStrassenBlockingAllFusedRotatedCustom(size, A22, S8, M7, buf);

	/*printf("size = %d\n", size);
	printOpsMatrixCounts(size, M1);
	printOpsMatrixCounts(size, M2);
	printOpsMatrixCounts(size, M3);
	printOpsMatrixCounts(size, M4);
	printOpsMatrixCounts(size, M5);
	printOpsMatrixCounts(size, M6);
	printOpsMatrixCounts(size, M7);*/

	//generate all combinations of the 7 multiplications and find
	//the permutations for each multiplications that will yield the
	//resultant matrix with the min std. dev.
	if (size != BLOCK) {
		int a, b, c, d, e, f, g;
		int result[7];
		int resulttemp[7];
		int tempsize = size >> 1;
		ops** M1temp = createOpsArray(size, size);
		ops** M2temp = createOpsArray(size, size);
		ops** M3temp = createOpsArray(size, size);
		ops** M4temp = createOpsArray(size, size);
		ops** M5temp = createOpsArray(size, size);
		ops** M6temp = createOpsArray(size, size);
		ops** M7temp = createOpsArray(size, size);
		ops** T1temp = createOpsArray(size, size);
		ops** T2temp = createOpsArray(size, size);
		ops** T3temp = createOpsArray(size, size);
		ops** Ctemp = createOpsArray(n, n);

		indexi[0] = 0;
		indexi[1] = 0;
		indexi[2] = tempsize;
		indexi[3] = tempsize;

		indexj[0] = 0;
		indexj[1] = tempsize;
		indexj[2] = 0;
		indexj[3] = tempsize;

		for (a = 0; a < 4; ++a) {
			result[0] = a;

			//get the permutation for M1
			for (i = 0; i < 12; ++i) {
				thisPermutation[i] = permuatations[result[0]][i];
			}

			//apply the permutation for M1
			for (i = 0; i < tempsize; i++) {
				for (j = 0; j < tempsize; j++) {
					M1temp[i][j].adds = M1[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].adds;
					M1temp[i][j].muls = M1[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].muls;
					M1temp[i][j].fusedDP = M1[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].fusedDP;
				}
			}

			for (i = 0; i < tempsize; i++) {
				for (j = tempsize; j < size; j++) {
					M1temp[i][j].adds = M1[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].adds;
					M1temp[i][j].muls = M1[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].muls;
					M1temp[i][j].fusedDP = M1[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].fusedDP;
				}
			}

			for (i = tempsize; i < size; i++) {
				for (j = 0; j < tempsize; j++) {
					M1temp[i][j].adds = M1[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].adds;
					M1temp[i][j].muls = M1[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].muls;
					M1temp[i][j].fusedDP = M1[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].fusedDP;
				}
			}

			for (i = tempsize; i < size; i++) {
				for (j = tempsize; j < size; j++) {
					M1temp[i][j].adds = M1[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].adds;
					M1temp[i][j].muls = M1[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].muls;
					M1temp[i][j].fusedDP = M1[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].fusedDP;
				}
			}

			for (b = 0; b < 4; ++b) {
				result[1] = b;

				//get the permutation for M2
				for (i = 0; i < 12; ++i) {
					thisPermutation[i] = permuatations[result[1]][i];
				}

				//apply the permutation for M2
				for (i = 0; i < tempsize; i++) {
					for (j = 0; j < tempsize; j++) {
						M2temp[i][j].adds = M2[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].adds;
						M2temp[i][j].muls = M2[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].muls;
						M2temp[i][j].fusedDP = M2[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].fusedDP;
					}
				}

				for (i = 0; i < tempsize; i++) {
					for (j = tempsize; j < size; j++) {
						M2temp[i][j].adds = M2[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].adds;
						M2temp[i][j].muls = M2[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].muls;
						M2temp[i][j].fusedDP = M2[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].fusedDP;
					}
				}

				for (i = tempsize; i < size; i++) {
					for (j = 0; j < tempsize; j++) {
						M2temp[i][j].adds = M2[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].adds;
						M2temp[i][j].muls = M2[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].muls;
						M2temp[i][j].fusedDP = M2[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].fusedDP;
					}
				}

				for (i = tempsize; i < size; i++) {
					for (j = tempsize; j < size; j++) {
						M2temp[i][j].adds = M2[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].adds;
						M2temp[i][j].muls = M2[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].muls;
						M2temp[i][j].fusedDP = M2[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].fusedDP;
					}
				}

				for (c = 0; c < 4; ++c) {
					result[2] = c;

					//get the permutation for M3
					for (i = 0; i < 12; ++i) {
						thisPermutation[i] = permuatations[result[2]][i];
					}

					//apply the permutation for M3
					for (i = 0; i < tempsize; i++) {
						for (j = 0; j < tempsize; j++) {
							M3temp[i][j].adds = M3[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].adds;
							M3temp[i][j].muls = M3[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].muls;
							M3temp[i][j].fusedDP = M3[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].fusedDP;
						}
					}

					for (i = 0; i < tempsize; i++) {
						for (j = tempsize; j < size; j++) {
							M3temp[i][j].adds = M3[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].adds;
							M3temp[i][j].muls = M3[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].muls;
							M3temp[i][j].fusedDP = M3[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].fusedDP;
						}
					}

					for (i = tempsize; i < size; i++) {
						for (j = 0; j < tempsize; j++) {
							M3temp[i][j].adds = M3[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].adds;
							M3temp[i][j].muls = M3[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].muls;
							M3temp[i][j].fusedDP = M3[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].fusedDP;
						}
					}

					for (i = tempsize; i < size; i++) {
						for (j = tempsize; j < size; j++) {
							M3temp[i][j].adds = M3[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].adds;
							M3temp[i][j].muls = M3[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].muls;
							M3temp[i][j].fusedDP = M3[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].fusedDP;
						}
					}

					for (d = 0; d < 4; ++d) {
						result[3] = d;

						//get the permutation for M4
						for (i = 0; i < 12; ++i) {
							thisPermutation[i] = permuatations[result[3]][i];
						}

						//apply the permutation for M4
						for (i = 0; i < tempsize; i++) {
							for (j = 0; j < tempsize; j++) {
								M4temp[i][j].adds = M4[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].adds;
								M4temp[i][j].muls = M4[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].muls;
								M4temp[i][j].fusedDP = M4[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].fusedDP;
							}
						}

						for (i = 0; i < tempsize; i++) {
							for (j = tempsize; j < size; j++) {
								M4temp[i][j].adds = M4[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].adds;
								M4temp[i][j].muls = M4[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].muls;
								M4temp[i][j].fusedDP = M4[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].fusedDP;
							}
						}

						for (i = tempsize; i < size; i++) {
							for (j = 0; j < tempsize; j++) {
								M4temp[i][j].adds = M4[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].adds;
								M4temp[i][j].muls = M4[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].muls;
								M4temp[i][j].fusedDP = M4[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].fusedDP;
							}
						}

						for (i = tempsize; i < size; i++) {
							for (j = tempsize; j < size; j++) {
								M4temp[i][j].adds = M4[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].adds;
								M4temp[i][j].muls = M4[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].muls;
								M4temp[i][j].fusedDP = M4[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].fusedDP;
							}
						}

						for (e = 0; e < 4; ++e) {
							result[4] = e;

							//get the permutation for M5
							for (i = 0; i < 12; ++i) {
								thisPermutation[i] =
										permuatations[result[4]][i];
							}

							//apply the permutation for M5
							for (i = 0; i < tempsize; i++) {
								for (j = 0; j < tempsize; j++) {
									M5temp[i][j].adds = M5[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].adds;
									M5temp[i][j].muls = M5[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].muls;
									M5temp[i][j].fusedDP = M5[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].fusedDP;
								}
							}

							for (i = 0; i < tempsize; i++) {
								for (j = tempsize; j < size; j++) {
									M5temp[i][j].adds = M5[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].adds;
									M5temp[i][j].muls = M5[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].muls;
									M5temp[i][j].fusedDP = M5[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].fusedDP;
								}
							}

							for (i = tempsize; i < size; i++) {
								for (j = 0; j < tempsize; j++) {
									M5temp[i][j].adds = M5[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].adds;
									M5temp[i][j].muls = M5[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].muls;
									M5temp[i][j].fusedDP = M5[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].fusedDP;
								}
							}

							for (i = tempsize; i < size; i++) {
								for (j = tempsize; j < size; j++) {
									M5temp[i][j].adds = M5[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].adds;
									M5temp[i][j].muls = M5[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].muls;
									M5temp[i][j].fusedDP = M5[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].fusedDP;
								}
							}

							for (f = 0; f < 4; ++f) {
								result[5] = f;

								//get the permutation for M6
								for (i = 0; i < 12; ++i) {
									thisPermutation[i] =
											permuatations[result[5]][i];
								}

								//apply the permutation for M6
								for (i = 0; i < tempsize; i++) {
									for (j = 0; j < tempsize; j++) {
										M6temp[i][j].adds = M6[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].adds;
										M6temp[i][j].muls = M6[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].muls;
										M6temp[i][j].fusedDP = M6[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].fusedDP;
									}
								}

								for (i = 0; i < tempsize; i++) {
									for (j = tempsize; j < size; j++) {
										M6temp[i][j].adds = M6[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].adds;
										M6temp[i][j].muls = M6[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].muls;
										M6temp[i][j].fusedDP = M6[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].fusedDP;
									}
								}

								for (i = tempsize; i < size; i++) {
									for (j = 0; j < tempsize; j++) {
										M6temp[i][j].adds = M6[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].adds;
										M6temp[i][j].muls = M6[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].muls;
										M6temp[i][j].fusedDP = M6[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].fusedDP;
									}
								}

								for (i = tempsize; i < size; i++) {
									for (j = tempsize; j < size; j++) {
										M6temp[i][j].adds = M6[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].adds;
										M6temp[i][j].muls = M6[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].muls;
										M6temp[i][j].fusedDP = M6[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].fusedDP;
									}
								}

								for (g = 0; g < 4; ++g) {
									result[6] = g;

									//get the permutation for M7
									for (i = 0; i < 12; ++i) {
										thisPermutation[i] =
												permuatations[result[6]][i];
									}

									//apply the permutation for M7
									for (i = 0; i < tempsize; i++) {
										for (j = 0; j < tempsize; j++) {
											M7temp[i][j].adds = M7[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].adds;
											M7temp[i][j].muls = M7[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].muls;
											M7temp[i][j].fusedDP = M7[indexi[thisPermutation[8] - 1] + i][indexj[thisPermutation[8] - 1] + j].fusedDP;
										}
									}

									for (i = 0; i < tempsize; i++) {
										for (j = tempsize; j < size; j++) {
											M7temp[i][j].adds = M7[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].adds;
											M7temp[i][j].muls = M7[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].muls;
											M7temp[i][j].fusedDP = M7[indexi[thisPermutation[9] - 1] + i][indexj[thisPermutation[9] - 1] + j - tempsize].fusedDP;
										}
									}

									for (i = tempsize; i < size; i++) {
										for (j = 0; j < tempsize; j++) {
											M7temp[i][j].adds = M7[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].adds;
											M7temp[i][j].muls = M7[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].muls;
											M7temp[i][j].fusedDP = M7[indexi[thisPermutation[10] - 1] + i - tempsize][indexj[thisPermutation[10] - 1] + j].fusedDP;
										}
									}

									for (i = tempsize; i < size; i++) {
										for (j = tempsize; j < size; j++) {
											M7temp[i][j].adds = M7[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].adds;
											M7temp[i][j].muls = M7[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].muls;
											M7temp[i][j].fusedDP = M7[indexi[thisPermutation[11] - 1] + i - tempsize][indexj[thisPermutation[11] - 1] + j - tempsize].fusedDP;
										}
									}

									//get the result matrix
									matrixAdd(M1temp, M2temp, C11, size);
									matrixAdd(M1temp, M4temp, T1temp, size);
									matrixAdd(T1temp, M5temp, T2temp, size);
									matrixAdd(T2temp, M7temp, C21, size);
									matrixAdd(T2temp, M3temp, C22, size);
									matrixAdd(T1temp, M3temp, T3temp, size);
									matrixAdd(T3temp, M6temp, C12, size);

								    combineMatrices(C11, C12, C21, C22, n, C);

									double temp_max;

									temp_max = get99percentileAddMulCount(n, C);

									if (max > temp_max) {
										max = temp_max;
										copyMatrix(n, C, Ctemp);
										//save permutation combination
										for (i = 0; i < 7; ++i) {
											resulttemp[i] = result[i];
										}
									} else if (max == temp_max) {
										if (breaktie(n, C, Ctemp)) {
											copyMatrix(n, C, Ctemp);
											for (i = 0; i < 7; ++i) {
												resulttemp[i] = result[i];
											}
										}
									}

								}
							}
						}
					}
				}
			}
		}

		//print out permutation combination with minimum SD
		printf("size = %d, multiplication = %s, permutation = {", n, mat_name);
		for (i = 0; i < 7; ++i) {
			printf("%d, ", resulttemp[i]);
		}
		printf("}\n");

		copyMatrix(n, Ctemp, C);
		//printOpsMatrix(n, C);
		printOpsMatrixCounts(n, C);

		destroyArray(M1temp);
		destroyArray(M2temp);
		destroyArray(M3temp);
		destroyArray(M4temp);
		destroyArray(M5temp);
		destroyArray(M6temp);
		destroyArray(M7temp);
		destroyArray(Ctemp);
	} else {
		matrixAdd(M1, M2, C11, size);
		matrixAdd(M1, M4, T1, size);
		matrixAdd(T1, M5, T2, size);
		matrixAdd(T2, M7, C21, size);
		matrixAdd(T2, M3, C22, size);
		matrixAdd(T1, M3, T3, size);
		matrixAdd(T3, M6, C12, size);

		if (thisPermutation[8] == 1 && thisPermutation[9] == 2
				&& thisPermutation[10] == 3 && thisPermutation[11] == 4) {
			combineMatrices(C11, C12, C21, C22, n, C);
		} else if (thisPermutation[8] == 3 && thisPermutation[9] == 4
				&& thisPermutation[10] == 1 && thisPermutation[11] == 2) {
			combineMatrices(C21, C22, C11, C12, n, C);
		} else if (thisPermutation[8] == 2 && thisPermutation[9] == 1
				&& thisPermutation[10] == 4 && thisPermutation[11] == 3) {
			combineMatrices(C12, C11, C22, C21, n, C);
		} else {
			combineMatrices(C22, C21, C12, C11, n, C);
		}
	}

	destroyArray(A11);
	destroyArray(A12);
	destroyArray(A21);
	destroyArray(A22);
	destroyArray(B11);
	destroyArray(B12);
	destroyArray(B21);
	destroyArray(B22);
	destroyArray(C11);
	destroyArray(C12);
	destroyArray(C21);
	destroyArray(C22);
	destroyArray(S1);
	destroyArray(S2);
	destroyArray(S3);
	destroyArray(S4);
	destroyArray(S5);
	destroyArray(S6);
	destroyArray(S7);
	destroyArray(S8);
	destroyArray(M1);
	destroyArray(M2);
	destroyArray(M3);
	destroyArray(M4);
	destroyArray(M5);
	destroyArray(M6);
	destroyArray(M7);
	destroyArray(T1);
	destroyArray(T2);
	destroyArray(T3);
}

void matMulStrassenBlockingAllFusedRotated(int n, ops** A, ops** B, ops** C) {
	int i, j;
	int thisPermutation[12];
	int indexi[4];
	int indexj[4];

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	//select the current permutation
	for (i = 0; i < 12; ++i) {
		thisPermutation[i] = permuatations[currentPermutationIndex][i];
	}
	//printf("Perm index = %d\n", currentPermutationIndex);

/*	if (currentPermutationIndex == 7)
		currentPermutationIndex = 0;
	else
		currentPermutationIndex++;*/

	indexi[0] = 0;
	indexi[1] = 0;
	indexi[2] = size;
	indexi[3] = size;

	indexj[0] = 0;
	indexj[1] = size;
	indexj[2] = 0;
	indexj[3] = size;

	ops** A11 = createOpsArray(size, size);
	ops** A12 = createOpsArray(size, size);
	ops** A21 = createOpsArray(size, size);
	ops** A22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j].adds =
					A[indexi[thisPermutation[0] - 1] + i][indexj[thisPermutation[0]
							- 1] + j].adds;
			A11[i][j].muls =
					A[indexi[thisPermutation[0] - 1] + i][indexj[thisPermutation[0]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j].adds =
					A[indexi[thisPermutation[1] - 1] + i][indexj[thisPermutation[1]
							- 1] + j].adds;
			A12[i][j].muls =
					A[indexi[thisPermutation[1] - 1] + i][indexj[thisPermutation[1]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j].adds =
					A[indexi[thisPermutation[2] - 1] + i][indexj[thisPermutation[2]
							- 1] + j].adds;
			A21[i][j].muls =
					A[indexi[thisPermutation[2] - 1] + i][indexj[thisPermutation[2]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j].adds =
					A[indexi[thisPermutation[3] - 1] + i][indexj[thisPermutation[3]
							- 1] + j].adds;
			A22[i][j].muls =
					A[indexi[thisPermutation[3] - 1] + i][indexj[thisPermutation[3]
							- 1] + j].muls;
		}
	}

	ops** B11 = createOpsArray(size, size);
	ops** B12 = createOpsArray(size, size);
	ops** B21 = createOpsArray(size, size);
	ops** B22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j].adds =
					B[indexi[thisPermutation[4] - 1] + i][indexj[thisPermutation[4]
							- 1] + j].adds;
			B11[i][j].muls =
					B[indexi[thisPermutation[4] - 1] + i][indexj[thisPermutation[4]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j].adds =
					B[indexi[thisPermutation[5] - 1] + i][indexj[thisPermutation[5]
							- 1] + j].adds;
			B12[i][j].muls =
					B[indexi[thisPermutation[5] - 1] + i][indexj[thisPermutation[5]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j].adds =
					B[indexi[thisPermutation[6] - 1] + i][indexj[thisPermutation[6]
							- 1] + j].adds;
			B21[i][j].muls =
					B[indexi[thisPermutation[6] - 1] + i][indexj[thisPermutation[6]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j].adds =
					B[indexi[thisPermutation[7] - 1] + i][indexj[thisPermutation[7]
							- 1] + j].adds;
			B22[i][j].muls =
					B[indexi[thisPermutation[7] - 1] + i][indexj[thisPermutation[7]
							- 1] + j].muls;
		}
	}

	ops** S1 = createOpsArray(size, size);
	ops** S2 = createOpsArray(size, size);
	ops** S3 = createOpsArray(size, size);
	ops** S4 = createOpsArray(size, size);
	ops** S5 = createOpsArray(size, size);
	ops** S6 = createOpsArray(size, size);
	ops** S7 = createOpsArray(size, size);
	ops** S8 = createOpsArray(size, size);
	ops** M1 = createOpsArray(size, size);
	ops** M2 = createOpsArray(size, size);
	ops** M3 = createOpsArray(size, size);
	ops** M4 = createOpsArray(size, size);
	ops** M5 = createOpsArray(size, size);
	ops** M6 = createOpsArray(size, size);
	ops** M7 = createOpsArray(size, size);
	ops** T2 = createOpsArray(size, size);
	ops** T3 = createOpsArray(size, size);
	ops** T6 = createOpsArray(size, size);
	ops** C11 = createOpsArray(size, size);
	ops** C12 = createOpsArray(size, size);
	ops** C21 = createOpsArray(size, size);
	ops** C22 = createOpsArray(size, size);
	ops** temp = createOpsArray(size, size);
	ops** temp2 = createOpsArray(size, size);

	matrixAdd(A21, A22, S1, size);
	negateUboundMatrix(temp, A11, size);
	matrixAddWithTwoFusedOps(A21, A22, temp, S2, size);
	matrixSub(A11, A21, S3, size);
	negateUboundMatrix(temp, A21, size);
	negateUboundMatrix(temp2, A22, size);
	matrixAddWithThreeFusedOps(A12, temp, temp2, A11, S4, size);
	matrixSub(B12, B11, S5, size);
	negateUboundMatrix(temp, B12, size);
	matrixAddWithTwoFusedOps(B22, temp, B11, S6, size);
	matrixSub(B22, B12, S7, size);
	negateUboundMatrix(temp, B22, size);
	negateUboundMatrix(temp2, B11, size);
	matrixAddWithThreeFusedOps(B21, temp, B12, temp2, S8, size);
	destroyArray(temp);
	destroyArray(temp2);

	matMulStrassenBlockingAllFusedRotated(size, A11, B11, M1);
	matMulStrassenBlockingAllFusedRotated(size, A12, B21, M2);
	matMulStrassenBlockingAllFusedRotated(size, S1, S5, M3);
	matMulStrassenBlockingAllFusedRotated(size, S2, S6, M4);
	matMulStrassenBlockingAllFusedRotated(size, S3, S7, M5);
	matMulStrassenBlockingAllFusedRotated(size, S4, B22, M6);
	matMulStrassenBlockingAllFusedRotated(size, A22, S8, M7);

	printf("size = %d\n", size);
	printOpsMatrix(size, M1);
	printOpsMatrix(size, M2);
	printOpsMatrix(size, M3);
	printOpsMatrix(size, M4);
	printOpsMatrix(size, M5);
	printOpsMatrix(size, M6);
	printOpsMatrix(size, M7);

	matrixAdd(M1, M2, C11, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M7, C21, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M3, C22, size);
	matrixAddWithThreeFusedOps(M1, M4, M3, M6, C12, size);

	if (thisPermutation[8] == 1 && thisPermutation[9] == 2
			&& thisPermutation[10] == 3 && thisPermutation[11] == 4) {
		combineMatrices(C11, C12, C21, C22, n, C);
	} else if (thisPermutation[8] == 3 && thisPermutation[9] == 4
			&& thisPermutation[10] == 1 && thisPermutation[11] == 2) {
		combineMatrices(C21, C22, C11, C12, n, C);
	} else if (thisPermutation[8] == 2 && thisPermutation[9] == 1
			&& thisPermutation[10] == 4 && thisPermutation[11] == 3) {
		combineMatrices(C12, C11, C22, C21, n, C);
	} else {
		combineMatrices(C22, C21, C12, C11, n, C);
	}

	/*printf("size = %d\n", size);
	printOpsMatrix(size, C11);
	printOpsMatrix(size, C12);
	printOpsMatrix(size, C21);
	printOpsMatrix(size, C22);
*/
	destroyArray(A11);
	destroyArray(A12);
	destroyArray(A21);
	destroyArray(A22);
	destroyArray(B11);
	destroyArray(B12);
	destroyArray(B21);
	destroyArray(B22);
	destroyArray(C11);
	destroyArray(C12);
	destroyArray(C21);
	destroyArray(C22);
	destroyArray(S1);
	destroyArray(S2);
	destroyArray(S3);
	destroyArray(S4);
	destroyArray(S5);
	destroyArray(S6);
	destroyArray(S7);
	destroyArray(S8);
	destroyArray(M1);
	destroyArray(M2);
	destroyArray(M3);
	destroyArray(M4);
	destroyArray(M5);
	destroyArray(M6);
	destroyArray(M7);
	destroyArray(T2);
	destroyArray(T3);
	destroyArray(T6);
}

void matMulStrassenBlockingAllFused(int n, ops** A, ops** B, ops** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ops** A11 = createOpsArray(size, size);
	ops** A12 = createOpsArray(size, size);
	ops** A21 = createOpsArray(size, size);
	ops** A22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j].adds = A[i][j].adds;
			A11[i][j].muls = A[i][j].muls;

			A12[i][j].adds = A[i][j + size].adds;
			A12[i][j].muls = A[i][j + size].muls;

			A21[i][j].adds = A[i + size][j].adds;
			A21[i][j].muls = A[i + size][j].muls;

			A22[i][j].adds = A[i + size][j + size].adds;
			A22[i][j].muls = A[i + size][j + size].muls;
		}
	}

	ops** B11 = createOpsArray(size, size);
	ops** B12 = createOpsArray(size, size);
	ops** B21 = createOpsArray(size, size);
	ops** B22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j].adds = B[i][j].adds;
			B11[i][j].muls = B[i][j].muls;

			B12[i][j].adds = B[i][j + size].adds;
			B12[i][j].muls = B[i][j + size].muls;

			B21[i][j].adds = B[i + size][j].adds;
			B21[i][j].muls = B[i + size][j].muls;

			B22[i][j].adds = B[i + size][j + size].adds;
			B22[i][j].muls = B[i + size][j + size].muls;
		}
	}

	ops** S1 = createOpsArray(size, size);
	ops** S2 = createOpsArray(size, size);
	ops** S3 = createOpsArray(size, size);
	ops** S4 = createOpsArray(size, size);
	ops** S5 = createOpsArray(size, size);
	ops** S6 = createOpsArray(size, size);
	ops** S7 = createOpsArray(size, size);
	ops** S8 = createOpsArray(size, size);
	ops** M1 = createOpsArray(size, size);
	ops** M2 = createOpsArray(size, size);
	ops** M3 = createOpsArray(size, size);
	ops** M4 = createOpsArray(size, size);
	ops** M5 = createOpsArray(size, size);
	ops** M6 = createOpsArray(size, size);
	ops** M7 = createOpsArray(size, size);
	ops** T2 = createOpsArray(size, size);
	ops** T3 = createOpsArray(size, size);
	ops** T6 = createOpsArray(size, size);
	ops** C11 = createOpsArray(size, size);
	ops** C12 = createOpsArray(size, size);
	ops** C21 = createOpsArray(size, size);
	ops** C22 = createOpsArray(size, size);
	ops** temp = createOpsArray(size, size);
	ops** temp2 = createOpsArray(size, size);

	matrixAdd(A21, A22, S1, size);
	negateUboundMatrix(temp, A11, size);
	matrixAddWithTwoFusedOps(A21, A22, temp, S2, size);
	matrixSub(A11, A21, S3, size);
	negateUboundMatrix(temp, A21, size);
	negateUboundMatrix(temp2, A22, size);
	matrixAddWithThreeFusedOps(A12, temp, temp2, A11, S4, size);
	matrixSub(B12, B11, S5, size);
	negateUboundMatrix(temp, B12, size);
	matrixAddWithTwoFusedOps(B22, temp, B11, S6, size);
	matrixSub(B22, B12, S7, size);
	negateUboundMatrix(temp, B22, size);
	negateUboundMatrix(temp2, B11, size);
	matrixAddWithThreeFusedOps(B21, temp, B12, temp2, S8, size);
	destroyArray(temp);
	destroyArray(temp2);

	matMulStrassenBlockingAllFused(size, A11, B11, M1);
	matMulStrassenBlockingAllFused(size, A12, B21, M2);
	matMulStrassenBlockingAllFused(size, S1, S5, M3);
	matMulStrassenBlockingAllFused(size, S2, S6, M4);
	matMulStrassenBlockingAllFused(size, S3, S7, M5);
	matMulStrassenBlockingAllFused(size, S4, B22, M6);
	matMulStrassenBlockingAllFused(size, A22, S8, M7);

	matrixAdd(M1, M2, C11, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M7, C21, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M3, C22, size);
	matrixAddWithThreeFusedOps(M1, M4, M3, M6, C12, size);
	combineMatrices(C11, C12, C21, C22, n, C);

	destroyArray(A11);
	destroyArray(A12);
	destroyArray(A21);
	destroyArray(A22);
	destroyArray(B11);
	destroyArray(B12);
	destroyArray(B21);
	destroyArray(B22);
	destroyArray(C11);
	destroyArray(C12);
	destroyArray(C21);
	destroyArray(C22);
	destroyArray(S1);
	destroyArray(S2);
	destroyArray(S3);
	destroyArray(S4);
	destroyArray(S5);
	destroyArray(S6);
	destroyArray(S7);
	destroyArray(S8);
	destroyArray(M1);
	destroyArray(M2);
	destroyArray(M3);
	destroyArray(M4);
	destroyArray(M5);
	destroyArray(M6);
	destroyArray(M7);
	destroyArray(T2);
	destroyArray(T3);
	destroyArray(T6);
}

void matrixAddWithTwoFusedOps(ops** A, ops** B, ops** C, ops** result, int size) {
	int i, j;
	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			result[i][j].adds = A[i][j].adds + B[i][j].adds + C[i][j].adds;
			result[i][j].muls = A[i][j].muls + B[i][j].muls + C[i][j].muls;
			result[i][j].fused3add++;
#ifdef OP_COUNT
			add_count++;
#endif
		}
	}
}

void negateUboundMatrix(ops** negated, ops** mat, int size) {
	int i, j;
	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			negated[i][j].adds = mat[i][j].adds;
			negated[i][j].muls = mat[i][j].muls;
		}
	}
}
void matrixAddWithThreeFusedOps(ops** A, ops** B, ops** C, ops** D,
		ops** result, int size) {
	int i, j;
	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			result[i][j].adds = A[i][j].adds + B[i][j].adds + C[i][j].adds + D[i][j].adds;
			result[i][j].muls = A[i][j].muls + B[i][j].muls + C[i][j].muls + D[i][j].muls;
			result[i][j].fused4add++;
#ifdef OP_COUNT
			add_count++;
#endif
		}
	}
}

void matrixSub(ops** A, ops** B, ops** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			//C[i][j] = A[i][j] - B[i][j];
			C[i][j].adds = A[i][j].adds + B[i][j].adds + 1;
			C[i][j].muls = A[i][j].muls + B[i][j].muls;
			C[i][j].fusedDP = A[i][j].fusedDP + B[i][j].fusedDP;
#ifdef OP_COUNT
			add_count++;
#endif
		}
	}
	return;
}

void matrixAdd(ops** A, ops** B, ops** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			//C[i][j] = A[i][j] + B[i][j];
			C[i][j].adds = A[i][j].adds + B[i][j].adds + 1;
			C[i][j].muls = A[i][j].muls + B[i][j].muls;
			C[i][j].fusedDP = A[i][j].fusedDP + B[i][j].fusedDP;
#ifdef OP_COUNT
			add_count++;
#endif
		}
	}
	return;
}

void combineMatrices(ops** C11, ops** C12, ops** C21, ops** C22, int n,
		ops** result) {
	int i, j;
	int size = n / 2;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i][j].adds = C11[i][j].adds;
			result[i][j].muls = C11[i][j].muls;
			result[i][j].fusedDP = C11[i][j].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i][j + size].adds = C12[i][j].adds;
			result[i][j + size].muls = C12[i][j].muls;
			result[i][j + size].fusedDP = C12[i][j].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i + size][j].adds = C21[i][j].adds;
			result[i + size][j].muls = C21[i][j].muls;
			result[i + size][j].fusedDP = C21[i][j].fusedDP;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i + size][j + size].adds = C22[i][j].adds;
			result[i + size][j + size].muls = C22[i][j].muls;
			result[i + size][j + size].fusedDP = C22[i][j].fusedDP;
		}
	}
	return;
}

void destroyArray(ops** arr) {
	free(*arr);
	free(arr);
}

void matMulStrassenBlockingRotated(int n, ops** A, ops** B, ops** C) {
	int i, j;
	int thisPermutation[12];
	int indexi[4];
	int indexj[4];

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFloat(n, A, B, C);
		return;
	}

	int size = n >> 1;

	//select the current permutation
	for (i = 0; i < 12; ++i) {
		thisPermutation[i] = permuatations[currentPermutationIndex][i];
	}
	//printf("Perm index = %d\n", currentPermutationIndex);

	if (currentPermutationIndex == 7)
		currentPermutationIndex = 0;
	else
		currentPermutationIndex++;

	indexi[0] = 0;
	indexi[1] = 0;
	indexi[2] = size;
	indexi[3] = size;

	indexj[0] = 0;
	indexj[1] = size;
	indexj[2] = 0;
	indexj[3] = size;

	ops** A11 = createOpsArray(size, size);
	ops** A12 = createOpsArray(size, size);
	ops** A21 = createOpsArray(size, size);
	ops** A22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j].adds =
					A[indexi[thisPermutation[0] - 1] + i][indexj[thisPermutation[0]
							- 1] + j].adds;
			A11[i][j].muls =
					A[indexi[thisPermutation[0] - 1] + i][indexj[thisPermutation[0]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j].adds =
					A[indexi[thisPermutation[1] - 1] + i][indexj[thisPermutation[1]
							- 1] + j].adds;
			A12[i][j].muls =
					A[indexi[thisPermutation[1] - 1] + i][indexj[thisPermutation[1]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j].adds =
					A[indexi[thisPermutation[2] - 1] + i][indexj[thisPermutation[2]
							- 1] + j].adds;
			A21[i][j].muls =
					A[indexi[thisPermutation[2] - 1] + i][indexj[thisPermutation[2]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j].adds =
					A[indexi[thisPermutation[3] - 1] + i][indexj[thisPermutation[3]
							- 1] + j].adds;
			A22[i][j].muls =
					A[indexi[thisPermutation[3] - 1] + i][indexj[thisPermutation[3]
							- 1] + j].muls;
		}
	}

	ops** B11 = createOpsArray(size, size);
	ops** B12 = createOpsArray(size, size);
	ops** B21 = createOpsArray(size, size);
	ops** B22 = createOpsArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j].adds =
					B[indexi[thisPermutation[4] - 1] + i][indexj[thisPermutation[4]
							- 1] + j].adds;
			B11[i][j].muls =
					B[indexi[thisPermutation[4] - 1] + i][indexj[thisPermutation[4]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j].adds =
					B[indexi[thisPermutation[5] - 1] + i][indexj[thisPermutation[5]
							- 1] + j].adds;
			B12[i][j].muls =
					B[indexi[thisPermutation[5] - 1] + i][indexj[thisPermutation[5]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j].adds =
					B[indexi[thisPermutation[6] - 1] + i][indexj[thisPermutation[6]
							- 1] + j].adds;
			B21[i][j].muls =
					B[indexi[thisPermutation[6] - 1] + i][indexj[thisPermutation[6]
							- 1] + j].muls;
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j].adds =
					B[indexi[thisPermutation[7] - 1] + i][indexj[thisPermutation[7]
							- 1] + j].adds;
			B22[i][j].muls =
					B[indexi[thisPermutation[7] - 1] + i][indexj[thisPermutation[7]
							- 1] + j].muls;
		}
	}

	ops** S1 = createOpsArray(size, size);
	ops** S2 = createOpsArray(size, size);
	ops** S3 = createOpsArray(size, size);
	ops** S4 = createOpsArray(size, size);
	ops** S5 = createOpsArray(size, size);
	ops** S6 = createOpsArray(size, size);
	ops** S7 = createOpsArray(size, size);
	ops** S8 = createOpsArray(size, size);
	ops** M1 = createOpsArray(size, size);
	ops** M2 = createOpsArray(size, size);
	ops** M3 = createOpsArray(size, size);
	ops** M4 = createOpsArray(size, size);
	ops** M5 = createOpsArray(size, size);
	ops** M6 = createOpsArray(size, size);
	ops** M7 = createOpsArray(size, size);
	ops** T2 = createOpsArray(size, size);
	ops** T3 = createOpsArray(size, size);
	ops** T6 = createOpsArray(size, size);
	ops** C11 = createOpsArray(size, size);
	ops** C12 = createOpsArray(size, size);
	ops** C21 = createOpsArray(size, size);
	ops** C22 = createOpsArray(size, size);

	matrixAdd(A21, A22, S1, size);
	matrixSub(S1, A11, S2, size);
	matrixSub(A11, A21, S3, size);
	matrixSub(A12, S2, S4, size);
	matrixSub(B12, B11, S5, size);
	matrixSub(B22, S5, S6, size);
	matrixSub(B22, B12, S7, size);
	matrixSub(B21, S6, S8, size);
	matMulStrassenBlockingRotated(size, A11, B11, M1);
	matMulStrassenBlockingRotated(size, A12, B21, M2);
	matMulStrassenBlockingRotated(size, S1, S5, M3);
	matMulStrassenBlockingRotated(size, S2, S6, M4);
	matMulStrassenBlockingRotated(size, S3, S7, M5);
	matMulStrassenBlockingRotated(size, S4, B22, M6);
	matMulStrassenBlockingRotated(size, A22, S8, M7);
	matrixAdd(M1, M2, C11, size);
	matrixAdd(M1, M4, T2, size);
	matrixAdd(T2, M5, T3, size);
	matrixAdd(T3, M7, C21, size);
	matrixAdd(T3, M3, C22, size);
	matrixAdd(T2, M3, T6, size);
	matrixAdd(T6, M6, C12, size);

	if (thisPermutation[8] == 1 && thisPermutation[9] == 2
			&& thisPermutation[10] == 3 && thisPermutation[11] == 4) {
		combineMatrices(C11, C12, C21, C22, n, C);
	} else if (thisPermutation[8] == 3 && thisPermutation[9] == 4
			&& thisPermutation[10] == 1 && thisPermutation[11] == 2) {
		combineMatrices(C21, C22, C11, C12, n, C);
	} else if (thisPermutation[8] == 2 && thisPermutation[9] == 1
			&& thisPermutation[10] == 4 && thisPermutation[11] == 3) {
		combineMatrices(C12, C11, C22, C21, n, C);
	} else {
		combineMatrices(C22, C21, C12, C11, n, C);
	}

	destroyArray(A11);
	destroyArray(A12);
	destroyArray(A21);
	destroyArray(A22);
	destroyArray(B11);
	destroyArray(B12);
	destroyArray(B21);
	destroyArray(B22);
	destroyArray(C11);
	destroyArray(C12);
	destroyArray(C21);
	destroyArray(C22);
	destroyArray(S1);
	destroyArray(S2);
	destroyArray(S3);
	destroyArray(S4);
	destroyArray(S5);
	destroyArray(S6);
	destroyArray(S7);
	destroyArray(S8);
	destroyArray(M1);
	destroyArray(M2);
	destroyArray(M3);
	destroyArray(M4);
	destroyArray(M5);
	destroyArray(M6);
	destroyArray(M7);
	destroyArray(T2);
	destroyArray(T3);
	destroyArray(T6);
}

void reset_counts() {
	add_count = 0;
	mul_count = 0;
}

void printMatrix(size_t n, float** mat) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%.57f\t", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void printOpsMatrix(size_t n, ops** mat) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%d\t", mat[i][j].adds + mat[i][j].muls);
		}
		printf("\n");
	}
	printf("\n");
}

void printOpsMatrixCounts(size_t n, ops** mat) {
	int i, j;
	printf("Printing {adds, muls, fusedDP}\n");
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("{%d, %d, %d}\t", mat[i][j].adds, mat[i][j].muls, mat[i][j].fusedDP);
		}
		printf("\n");
	}
	printf("\n");
}

void printToFile(ops** matrix, char* file_name) {
	FILE *f;
	int i, j;

	f = fopen(file_name, "w");
	fprintf(f, file_name);
	fprintf(f, "\n");

	for (i = 0; i < MAT_SIZE; i++) {
		for (j = 0; j < MAT_SIZE; j++) {
			fprintf(f, "%d, ", (matrix[i][j].adds + matrix[i][j].muls));
		}
	}
	fclose(f);
}

void copyMatrix(int size, ops** mat1, ops** mat2) {
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			mat2[i][j].adds = mat1[i][j].adds;
			mat2[i][j].muls = mat1[i][j].muls;
		}
	}
}

double getStdDevTotalOpCount(int size, ops** mat) {
	double ans = 0, mean = 0, totalops;
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			totalops = mat[i][j].adds + mat[i][j].muls;
			mean += totalops;
		}
	}

	mean /= (size * size);

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			totalops = mat[i][j].adds + mat[i][j].muls;
			ans += pow((totalops - mean), 2);
		}
	}

	ans = ans / (size * size);
	return sqrt(ans);
}

double getStdDevAddCount(int size, ops** mat) {
	double ans = 0, mean = 0, totalops;
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			mean += mat[i][j].adds;
		}
	}

	mean /= (size * size);

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			totalops = mat[i][j].adds;
			ans += pow((totalops - mean), 2);
		}
	}

	ans = ans / (size * size);
	return sqrt(ans);
}

int get90percentile(int size, ops** mat) {
	int *arr = malloc(sizeof(int) * size * size);
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			arr[i*size + j] = mat[i][j].adds + mat[i][j].muls;
		}
	}

	qsort(arr, size * size, sizeof(int), compare);

	int indexOf90 = (size * size) * 0.90;

	return arr[indexOf90];
}

int get99percentileAddCount(int size, ops** mat) {
	int *arr = malloc(sizeof(int) * size * size);
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			arr[i*size + j] = mat[i][j].adds;
		}
	}

	qsort(arr, size * size, sizeof(int), compare);

	int indexOf90 = (size * size) * 0.99;

	return arr[indexOf90];
}

int get99percentileAddMulCount(int size, ops** mat) {
	int *arr = malloc(sizeof(int) * size * size);
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			arr[i*size + j] = mat[i][j].adds + mat[i][j].muls + mat[i][j].fusedDP;
		}
	}

	qsort(arr, size * size, sizeof(int), compare);

	int indexOf90 = (size * size) * 0.99;
	indexOf90 = arr[indexOf90];
	free(arr);

	return indexOf90;
}

int breaktie(int size, ops** C, ops** Ctemp) {
	int i,j, returnvalue = 0;

	int *arr = malloc(sizeof(int) * size * size);
	int *arr2 = malloc(sizeof(int) * size * size);

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			arr[i * size + j] = C[i][j].adds + C[i][j].muls + C[i][j].fusedDP;
			arr2[i * size + j] = Ctemp[i][j].adds + Ctemp[i][j].muls + Ctemp[i][j].fusedDP;
		}
	}

	qsort(arr, size * size, sizeof(int), compare);
	qsort(arr2, size * size, sizeof(int), compare);

	for (i = (size * size) - 1; i >= 0; i--) {
		if (arr[i] < arr2[i]) {
			returnvalue = 1;
			break;
		} else if (arr[i] > arr2[i]) {
			returnvalue = 0;
			break;
		}
	}

	free(arr);
	free(arr2);

	return returnvalue;
}

int getMinCount(int size, ops** mat) {
	int *arr = malloc(sizeof(int) * size * size);
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			arr[i * size + j] = mat[i][j].adds;
		}
	}

	qsort(arr, size * size, sizeof(int), compare);

	return arr[0];
}

int get90percentileAddCount(int size, ops** mat) {
	int *arr = malloc(sizeof(int) * size * size);
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			arr[i*size + j] = mat[i][j].adds;
		}
	}

	qsort(arr, size * size, sizeof(int), compare);

	int indexOf90 = (size * size) * 0.90;
	printf("%d\n", arr[indexOf90]);

	return arr[indexOf90];
}

double getAvgOpCount(int size, ops** mat) {
	double total = 0;
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
#ifdef TOTALOPCOUNT
			total += mat[i][j].adds;
			total += mat[i][j].muls;
#endif
#ifdef ADDCOUNT
			total += mat[i][j].adds;
#endif
		}
	}

	return total/(size * size);
}

void clearMatrix(int size, ops** mat) {
	int i, j;
	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			mat[i][j].adds = 0;
			mat[i][j].muls = 0;
		}
	}
}

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

