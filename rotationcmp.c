#include <stdio.h>
#include <libunum.h>
#include <time.h>
#include <gmp.h>
#include <mpfr.h>

#define IS_POWER_OF_2(n) ((n != 0) && ((n & (n - 1)) == 0))
#define MAT_SIZE 128
#define REPEAT 1
#define LEVEL_FACTOR 4
#define BLOCK MAT_SIZE/LEVEL_FACTOR
#define MPFR_ROUNDING_MODE MPFR_RNDN
#define SD
#define REGULAR_ADDS
#define ESIZESIZE 3
#define FSIZESIZE 5
#define MPFR_PREC 23

int permuatations[8][12] = { {1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4},
		{3, 4, 1, 2, 1, 2, 3, 4, 3, 4, 1, 2},
		{1, 2, 3, 4, 2, 1, 4, 3, 2, 1, 4, 3},
		{4, 3, 2, 1, 4, 3, 2, 1, 4, 3, 2, 1},
		{2, 1, 4, 3, 3, 4, 1, 2, 1, 2, 3, 4},
		{4, 3, 2, 1, 3, 4, 1, 2, 3, 4, 1, 2},
		{2, 1, 4, 3, 4, 3, 2, 1, 2, 1, 4, 3},
		{3, 4, 1, 2, 2, 1, 4, 3, 4, 3, 2, 1} };
int currentPermutationIndex = 0;
int randomIndex, *randomArray;
int customIndex, *customArray;

void doMatMul(int mat_size);
ubound_t** createUboundArray(int rows, int columns);
double** createDoubleArray(int rows, int columns);
void assignRandomDoubleMatrix(double** matrix, int rows, int columns);
mpfr_t** createMPFRArray(int rows, int columns);
void assignDoubleToMPFRMatrix(double** matrixd, mpfr_t** matrixMPFR, int rows, int columns);
void assignMPFRToUboundMatrix(mpfr_t** matrixmpfr, ubound_t** matrixU, int rows, int columns);
void matMulTraditionalDouble(int n, double** A, double** B, double** C);
void matMulTraditionalU(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulTraditionalFusedU(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulStrassenUBlockingAllFused(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matrixAddU(ubound_t** A, ubound_t** B, ubound_t** C, int size);
void matrixSubU(ubound_t** A, ubound_t** B, ubound_t** C, int size);
void negateUboundMatrix(ubound_t** negated, ubound_t** mat, int size);
void matrixAddWithTwoFusedOps(ubound_t** A, ubound_t** B, ubound_t** C, ubound_t** result, int size);
void matrixAddWithThreeFusedOps(ubound_t** A, ubound_t** B, ubound_t** C, ubound_t** D, ubound_t** result, int size);
void combineMatricesU(ubound_t** C11, ubound_t** C12, ubound_t** C21, ubound_t** C22, int n, ubound_t** result);
double getUboundWidth(ubound_t ub);
void destroyArrayU(ubound_t** arr);
void destroyArrayMPFR(mpfr_t** arr, int size);
void destroyArrayD(double** arr);
void printToFile(double **error, char* file_name);
void matMulStrassenUBlockingAllFusedRoundRobin(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulStrassenUBlockingAllFusedRandom(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulStrassenUBlockingAllFusedCustom(int n, ubound_t** A, ubound_t** B, ubound_t** C, char* mat_name);
void printMatrixU(size_t n, ubound_t** mat, char* mat_name);
void printWidthMatrixU(size_t n, ubound_t** mat, char* mat_name);

int main() {
	srand(time(0));
	doMatMul(MAT_SIZE);
}

void doMatMul(int mat_size) {
	set_env(ESIZESIZE, FSIZESIZE);
	_g_fsizemax = MPFR_PREC;

	ubound_t **u1, **u2, **uTradResult, **uTradFusedResult, **uStrasBlockFused, **uStrasRoundRobinFused, **uStrasRandomFused, **uStrasCustomFused;
	double **eTradUnum, **eTradUnumFused, **eStrasBlockUnumFused, **euStrasRoundRobinUnumFused, **euStrasRandomUnumFused, **euStrasCustomUnumFused;
	double **d1, **d2, **dresult;
	mpfr_t **mpfr1, **mpfr2;
	int i, j, k, l;
	char buffer[100];

	u1 = createUboundArray(mat_size, mat_size);
	u2 = createUboundArray(mat_size, mat_size);
	uStrasBlockFused = createUboundArray(mat_size, mat_size);
	uStrasRoundRobinFused = createUboundArray(mat_size, mat_size);
	uStrasRandomFused = createUboundArray(mat_size, mat_size);
	uStrasCustomFused = createUboundArray(mat_size, mat_size);
	uTradResult = createUboundArray(mat_size, mat_size);
	uTradFusedResult = createUboundArray(mat_size, mat_size);

	// initialize all matrices
	eTradUnum = createDoubleArray((mat_size * mat_size), REPEAT);
	eTradUnumFused = createDoubleArray((mat_size * mat_size), REPEAT);
	eStrasBlockUnumFused = createDoubleArray((mat_size * mat_size), REPEAT);
	euStrasRoundRobinUnumFused = createDoubleArray((mat_size * mat_size), REPEAT);
	euStrasRandomUnumFused = createDoubleArray((mat_size * mat_size), REPEAT);
	euStrasCustomUnumFused = createDoubleArray((mat_size * mat_size), REPEAT);

	d1 = createDoubleArray(mat_size, mat_size);
	d2 = createDoubleArray(mat_size, mat_size);
	dresult = createDoubleArray(mat_size, mat_size);

	mpfr1 = createMPFRArray(mat_size, mat_size);
	mpfr2 = createMPFRArray(mat_size, mat_size);

	printf("Matrix size = %d\n", mat_size);
	printf("Block size = %d\n", BLOCK);
	printf("REPEAT = %d\n", REPEAT);

	//generate random indices depending on # of levels of recursion
	int levels = __builtin_ctz(LEVEL_FACTOR);
	int count = 0;
	float randomIndexMatrixSize = 0.0;
	for (count = 0; count < levels; count++) {
		randomIndexMatrixSize += pow(7.0, count);
	}
	count = (int) randomIndexMatrixSize;
	randomArray = (int *) malloc(sizeof(int) * count);
	for (l = 0; l < count; ++l) {
		randomArray[l] = rand() % 4;
		printf("randomArray[%d] = %d\n", l, randomArray[l]);
	}

	//generate custom array
	customArray = (int *) malloc(sizeof(int) * count);

#ifdef SD
	if (LEVEL_FACTOR == 4) {
		customArray[0] = 0;
		customArray[1] = 0;
		customArray[2] = 1;
		customArray[3] = 2;
		customArray[4] = 3;
		customArray[5] = 1;
		customArray[6] = 1;
		customArray[7] = 2;
	} else if (LEVEL_FACTOR == 8) {
		customArray[0] = 0;

		customArray[1] = 0;

		customArray[2] = 0;
		customArray[3] = 1;
		customArray[4] = 2;
		customArray[5] = 3;
		customArray[6] = 1;
		customArray[7] = 1;
		customArray[8] = 2;

		customArray[9] = 1;

		customArray[10] = 0;
		customArray[11] = 1;
		customArray[12] = 2;
		customArray[13] = 3;
		customArray[14] = 1;
		customArray[15] = 1;
		customArray[16] = 2;

		customArray[17] = 2;

		customArray[18] = 0;
		customArray[19] = 1;
		customArray[20] = 2;
		customArray[21] = 3;
		customArray[22] = 1;
		customArray[23] = 1;
		customArray[24] = 2;

		customArray[25] = 3;

		customArray[26] = 0;
		customArray[27] = 1;
		customArray[28] = 2;
		customArray[29] = 3;
		customArray[30] = 1;
		customArray[31] = 1;
		customArray[32] = 2;

		customArray[33] = 1;

		customArray[34] = 0;
		customArray[35] = 1;
		customArray[36] = 2;
		customArray[37] = 3;
		customArray[38] = 1;
		customArray[39] = 1;
		customArray[40] = 2;

		customArray[41] = 1;

		customArray[42] = 0;
		customArray[43] = 2;
		customArray[44] = 2;
		customArray[45] = 3;
		customArray[46] = 1;
		customArray[47] = 1;
		customArray[48] = 2;

		customArray[49] = 2;

		customArray[50] = 0;
		customArray[51] = 1;
		customArray[52] = 2;
		customArray[53] = 3;
		customArray[54] = 1;
		customArray[55] = 1;
		customArray[56] = 2;
	} else {
		printf("Custom Array not defined!\n");
	}

#endif

#ifdef PERCENTILE
	if (LEVEL_FACTOR == 4) {
		customArray[0] = 0;
		customArray[1] = 0;
		customArray[2] = 1;
		customArray[3] = 2;
		customArray[4] = 3;
		customArray[5] = 1;
		customArray[6] = 1;
		customArray[7] = 2;
	} else {
		printf("Custom Array not defined!\n");
	}
#endif

	for (i = 0; i < REPEAT; i++) {
		printf("i = %d\n", i);

		assignRandomDoubleMatrix(d1, mat_size, mat_size);
		assignRandomDoubleMatrix(d2, mat_size, mat_size);

		assignDoubleToMPFRMatrix(d1, mpfr1, mat_size, mat_size);
		assignDoubleToMPFRMatrix(d2, mpfr2, mat_size, mat_size);

		assignMPFRToUboundMatrix(mpfr1, u1, mat_size, mat_size);
		assignMPFRToUboundMatrix(mpfr2, u2, mat_size, mat_size);

		matMulStrassenUBlockingAllFused(mat_size, u1, u2, uStrasBlockFused);
		matMulTraditionalU(mat_size, u1, u2, uTradResult);

		//for round robin make sure the current index is always at 0 to ensure
		//that each iteration picks the same order of permutations
		/*currentPermutationIndex = 0;
		matMulStrassenUBlockingAllFusedRoundRobin(mat_size, u1, u2, uStrasRoundRobinFused);

		//for random rotation, the index of the random matrix indices should be reset to 0
		randomIndex = 0;
		matMulStrassenUBlockingAllFusedRandom(mat_size, u1, u2, uStrasRandomFused);*/

		//for custom rotation, reset the custom index to 0
		customIndex = 0;
		matMulStrassenUBlockingAllFusedCustom(mat_size, u1, u2, uStrasCustomFused, "initial");

		/*printMatrixU(mat_size, uTradResult, "trad");
		printMatrixU(mat_size, uStrasRoundRobinFused, "RR");
		printMatrixU(mat_size, uStrasRandomFused, "random");
		printMatrixU(mat_size, uTradResult, "uStrasCustomFused");
*/
		for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				eTradUnum[i][(j * mat_size) + k] = getUboundWidth(
						uTradResult[j][k]);
			}
		}

		for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				eStrasBlockUnumFused[i][(j * mat_size) + k] = getUboundWidth(
						uStrasBlockFused[j][k]);
			}
		}

	/*	for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				euStrasRoundRobinUnumFused[i][(j * mat_size) + k] = getUboundWidth(
						uStrasRoundRobinFused[j][k]);
			}
		}

		for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				euStrasRandomUnumFused[i][(j * mat_size) + k] =
						getUboundWidth(uStrasRandomFused[j][k]);
			}
		}*/

		for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				euStrasCustomUnumFused[i][(j * mat_size) + k] = getUboundWidth(
						uStrasCustomFused[j][k]);
			}
		}
	}

	destroyArrayD(d1);
	destroyArrayD(d2);
	destroyArrayD(dresult);
	destroyArrayU(u1);
	destroyArrayU(u2);
	destroyArrayU(uTradResult);
	destroyArrayU(uTradFusedResult);
	destroyArrayU(uStrasBlockFused);
	destroyArrayU(uStrasRoundRobinFused);
	destroyArrayU(uStrasRandomFused);
	destroyArrayU(uStrasCustomFused);
	destroyArrayMPFR(mpfr1, mat_size);
	destroyArrayMPFR(mpfr2, mat_size);

	snprintf(buffer, sizeof(char) * 100, "%dx%d", mat_size, mat_size);
	mkdir(buffer, 0700);

	printToFile(eStrasBlockUnumFused, "Stras_block_unum_fused.txt");
	printToFile(eTradUnum, "eTradUnum.txt");
	printToFile(euStrasCustomUnumFused, "Stras_block_unum_Custom_fused.txt");

	printf("size = %d, level = %d\n", MAT_SIZE, LEVEL_FACTOR);
	float counter = 0;
	for (i = 0; i < mat_size * mat_size; ++i) {
		if (euStrasRoundRobinUnumFused[0][i] >= eStrasBlockUnumFused[0][i]) {
			counter++;
		}
	}
	printf("round robin = %f\n", counter / (mat_size * mat_size));

	counter = 0;
	for (i = 0; i < mat_size * mat_size; ++i) {
		if (euStrasRandomUnumFused[0][i] >= eStrasBlockUnumFused[0][i]) {
			counter++;
		}
	}
	printf("random = %f\n", counter / (mat_size * mat_size));

	counter = 0;
	for (i = 0; i < mat_size * mat_size; ++i) {
		if (euStrasCustomUnumFused[0][i] >= eStrasBlockUnumFused[0][i]) {
			counter++;
		}
	}
	printf("heuristic = %f\n", counter / (mat_size * mat_size));
	printf("Done!\n");
}

void matMulStrassenUBlockingAllFusedCustom(int n, ubound_t** A, ubound_t** B,
		ubound_t** C, char* mat_name) {
	int i, j;
	int thisPermutation[12];
	int indexi[4];
	int indexj[4];
	char buf[100];

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		//matMulTraditionalU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	//select the current permutation
	currentPermutationIndex = customArray[customIndex];
	//printf("size = %d, multiplication = %s, permutation = %d\n", n, mat_name, currentPermutationIndex);
	customIndex++;
	for (i = 0; i < 12; ++i) {
		thisPermutation[i] = permuatations[currentPermutationIndex][i];
	}

	indexi[0] = 0;
	indexi[1] = 0;
	indexi[2] = size;
	indexi[3] = size;

	indexj[0] = 0;
	indexj[1] = size;
	indexj[2] = 0;
	indexj[3] = size;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] =
					A[indexi[thisPermutation[0] - 1] + i][indexj[thisPermutation[0]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j] =
					A[indexi[thisPermutation[1] - 1] + i][indexj[thisPermutation[1]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j] =
					A[indexi[thisPermutation[2] - 1] + i][indexj[thisPermutation[2]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j] =
					A[indexi[thisPermutation[3] - 1] + i][indexj[thisPermutation[3]
							- 1] + j];
		}
	}

	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j] =
					B[indexi[thisPermutation[4] - 1] + i][indexj[thisPermutation[4]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j] =
					B[indexi[thisPermutation[5] - 1] + i][indexj[thisPermutation[5]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j] =
					B[indexi[thisPermutation[6] - 1] + i][indexj[thisPermutation[6]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j] =
					B[indexi[thisPermutation[7] - 1] + i][indexj[thisPermutation[7]
							- 1] + j];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T1 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);
	ubound_t** temp = createUboundArray(size, size);
	ubound_t** temp2 = createUboundArray(size, size);

#ifdef FUSED_ADD
	//additions
	matrixAddU(A21, A22, S1, size);
	negateUboundMatrix(temp, A11, size);
	matrixAddWithTwoFusedOps(A21, A22, temp, S2, size);
	matrixSubU(A11, A21, S3, size);
	negateUboundMatrix(temp, A21, size);
	negateUboundMatrix(temp2, A22, size);
	matrixAddWithThreeFusedOps(A12, temp, temp2, A11, S4, size);
	matrixSubU(B12, B11, S5, size);
	negateUboundMatrix(temp, B12, size);
	matrixAddWithTwoFusedOps(B22, temp, B11, S6, size);
	matrixSubU(B22, B12, S7, size);
	negateUboundMatrix(temp, B22, size);
	negateUboundMatrix(temp2, B11, size);
	matrixAddWithThreeFusedOps(B21, temp, B12, temp2, S8, size);
	destroyArrayU(temp);
	destroyArrayU(temp2);
#endif

#ifdef REGULAR_ADDS
		matrixAddU(A21, A22, S1, size);
		matrixSubU(S1, A11, S2, size);
		matrixSubU(A11, A21, S3, size);
		matrixSubU(A12, S2, S4, size);
		matrixSubU(B12, B11, S5, size);
		matrixSubU(B22, S5, S6, size);
		matrixSubU(B22, B12, S7, size);
		matrixSubU(B21, S6, S8, size);
#endif

	//multiplications
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M1");
	matMulStrassenUBlockingAllFusedCustom(size, A11, B11, M1, "M1");
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M2");
	matMulStrassenUBlockingAllFusedCustom(size, A12, B21, M2, "M2");
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M3");
	matMulStrassenUBlockingAllFusedCustom(size, S1, S5, M3, "M3");
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M4");
	matMulStrassenUBlockingAllFusedCustom(size, S2, S6, M4, "M4");
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M5");
	matMulStrassenUBlockingAllFusedCustom(size, S3, S7, M5, "M5");
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M6");
	matMulStrassenUBlockingAllFusedCustom(size, S4, B22, M6, "M6");
	snprintf(buf, sizeof buf, "[%s] %d %s", mat_name, size, "M7");
	matMulStrassenUBlockingAllFusedCustom(size, A22, S8, M7, "M7");

	//additions
	matrixAddU(M1, M2, C11, size);
	matrixAddU(M1, M4, T1, size);
	matrixAddU(T1, M5, T2, size);
	matrixAddU(T2, M7, C21, size);
	matrixAddU(T2, M3, C22, size);
	matrixAddU(T1, M3, T3, size);
	matrixAddU(T3, M6, C12, size);
	if (thisPermutation[8] == 1 && thisPermutation[9] == 2
			&& thisPermutation[10] == 3 && thisPermutation[11] == 4) {
		combineMatricesU(C11, C12, C21, C22, n, C);
	} else if (thisPermutation[8] == 3 && thisPermutation[9] == 4
			&& thisPermutation[10] == 1 && thisPermutation[11] == 2) {
		combineMatricesU(C21, C22, C11, C12, n, C);
	} else if (thisPermutation[8] == 2 && thisPermutation[9] == 1
			&& thisPermutation[10] == 4 && thisPermutation[11] == 3) {
		combineMatricesU(C12, C11, C22, C21, n, C);
	} else {
		combineMatricesU(C22, C21, C12, C11, n, C);
	}

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T1);
	destroyArrayU(T2);
	destroyArrayU(T3);
}

void matMulStrassenUBlockingAllFusedRandom(int n, ubound_t** A, ubound_t** B,
		ubound_t** C) {
	int i, j;
	int thisPermutation[12];
	int indexi[4];
	int indexj[4];

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		//matMulTraditionalFusedU(n, A, B, C);
		matMulTraditionalU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	//select the current permutation
	currentPermutationIndex = randomArray[randomIndex];
	randomIndex++;
	for (i = 0; i < 12; ++i) {
		thisPermutation[i] = permuatations[currentPermutationIndex][i];
	}

	indexi[0] = 0;
	indexi[1] = 0;
	indexi[2] = size;
	indexi[3] = size;

	indexj[0] = 0;
	indexj[1] = size;
	indexj[2] = 0;
	indexj[3] = size;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] =
					A[indexi[thisPermutation[0] - 1] + i][indexj[thisPermutation[0]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j] =
					A[indexi[thisPermutation[1] - 1] + i][indexj[thisPermutation[1]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j] =
					A[indexi[thisPermutation[2] - 1] + i][indexj[thisPermutation[2]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j] =
					A[indexi[thisPermutation[3] - 1] + i][indexj[thisPermutation[3]
							- 1] + j];
		}
	}

	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j] =
					B[indexi[thisPermutation[4] - 1] + i][indexj[thisPermutation[4]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j] =
					B[indexi[thisPermutation[5] - 1] + i][indexj[thisPermutation[5]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j] =
					B[indexi[thisPermutation[6] - 1] + i][indexj[thisPermutation[6]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j] =
					B[indexi[thisPermutation[7] - 1] + i][indexj[thisPermutation[7]
							- 1] + j];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T1 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);
	ubound_t** temp = createUboundArray(size, size);
	ubound_t** temp2 = createUboundArray(size, size);

#ifdef FUSED_ADD
	//additions
	matrixAddU(A21, A22, S1, size);
	negateUboundMatrix(temp, A11, size);
	matrixAddWithTwoFusedOps(A21, A22, temp, S2, size);
	matrixSubU(A11, A21, S3, size);
	negateUboundMatrix(temp, A21, size);
	negateUboundMatrix(temp2, A22, size);
	matrixAddWithThreeFusedOps(A12, temp, temp2, A11, S4, size);
	matrixSubU(B12, B11, S5, size);
	negateUboundMatrix(temp, B12, size);
	matrixAddWithTwoFusedOps(B22, temp, B11, S6, size);
	matrixSubU(B22, B12, S7, size);
	negateUboundMatrix(temp, B22, size);
	negateUboundMatrix(temp2, B11, size);
	matrixAddWithThreeFusedOps(B21, temp, B12, temp2, S8, size);
	destroyArrayU(temp);
	destroyArrayU(temp2);
#endif

#ifdef REGULAR_ADDS
		matrixAddU(A21, A22, S1, size);
		matrixSubU(S1, A11, S2, size);
		matrixSubU(A11, A21, S3, size);
		matrixSubU(A12, S2, S4, size);
		matrixSubU(B12, B11, S5, size);
		matrixSubU(B22, S5, S6, size);
		matrixSubU(B22, B12, S7, size);
		matrixSubU(B21, S6, S8, size);
#endif

	//multiplications
	matMulStrassenUBlockingAllFusedRandom(size, A11, B11, M1);
	matMulStrassenUBlockingAllFusedRandom(size, A12, B21, M2);
	matMulStrassenUBlockingAllFusedRandom(size, S1, S5, M3);
	matMulStrassenUBlockingAllFusedRandom(size, S2, S6, M4);
	matMulStrassenUBlockingAllFusedRandom(size, S3, S7, M5);
	matMulStrassenUBlockingAllFusedRandom(size, S4, B22, M6);
	matMulStrassenUBlockingAllFusedRandom(size, A22, S8, M7);

	//additions
	matrixAddU(M1, M2, C11, size);
	matrixAddU(M1, M4, T1, size);
	matrixAddU(T1, M5, T2, size);
	matrixAddU(T2, M7, C21, size);
	matrixAddU(T2, M3, C22, size);
	matrixAddU(T1, M3, T3, size);
	matrixAddU(T3, M6, C12, size);
	if (thisPermutation[8] == 1 && thisPermutation[9] == 2
			&& thisPermutation[10] == 3 && thisPermutation[11] == 4) {
		combineMatricesU(C11, C12, C21, C22, n, C);
	} else if (thisPermutation[8] == 3 && thisPermutation[9] == 4
			&& thisPermutation[10] == 1 && thisPermutation[11] == 2) {
		combineMatricesU(C21, C22, C11, C12, n, C);
	} else if (thisPermutation[8] == 2 && thisPermutation[9] == 1
			&& thisPermutation[10] == 4 && thisPermutation[11] == 3) {
		combineMatricesU(C12, C11, C22, C21, n, C);
	} else {
		combineMatricesU(C22, C21, C12, C11, n, C);
	}

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T1);
	destroyArrayU(T2);
	destroyArrayU(T3);
}

void matMulStrassenUBlockingAllFusedRoundRobin(int n, ubound_t** A, ubound_t** B,
		ubound_t** C) {
	int i, j;
	int thisPermutation[12];
	int indexi[4];
	int indexj[4];

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		//matMulTraditionalFusedU(n, A, B, C);
		matMulTraditionalU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	//select the current permutation
	for (i = 0; i < 12; ++i) {
		thisPermutation[i] = permuatations[currentPermutationIndex][i];
	}

	if (currentPermutationIndex == 7)
		currentPermutationIndex = 0;
	else
		currentPermutationIndex++;

	indexi[0] = 0;
	indexi[1] = 0;
	indexi[2] = size;
	indexi[3] = size;

	indexj[0] = 0;
	indexj[1] = size;
	indexj[2] = 0;
	indexj[3] = size;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] =
					A[indexi[thisPermutation[0] - 1] + i][indexj[thisPermutation[0]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j] =
					A[indexi[thisPermutation[1] - 1] + i][indexj[thisPermutation[1]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j] =
					A[indexi[thisPermutation[2] - 1] + i][indexj[thisPermutation[2]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j] =
					A[indexi[thisPermutation[3] - 1] + i][indexj[thisPermutation[3]
							- 1] + j];
		}
	}

	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j] =
					B[indexi[thisPermutation[4] - 1] + i][indexj[thisPermutation[4]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j] =
					B[indexi[thisPermutation[5] - 1] + i][indexj[thisPermutation[5]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j] =
					B[indexi[thisPermutation[6] - 1] + i][indexj[thisPermutation[6]
							- 1] + j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j] =
					B[indexi[thisPermutation[7] - 1] + i][indexj[thisPermutation[7]
							- 1] + j];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T1 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);
	ubound_t** temp = createUboundArray(size, size);
	ubound_t** temp2 = createUboundArray(size, size);

#ifdef FUSED_ADD
	//additions
	matrixAddU(A21, A22, S1, size);
	negateUboundMatrix(temp, A11, size);
	matrixAddWithTwoFusedOps(A21, A22, temp, S2, size);
	matrixSubU(A11, A21, S3, size);
	negateUboundMatrix(temp, A21, size);
	negateUboundMatrix(temp2, A22, size);
	matrixAddWithThreeFusedOps(A12, temp, temp2, A11, S4, size);
	matrixSubU(B12, B11, S5, size);
	negateUboundMatrix(temp, B12, size);
	matrixAddWithTwoFusedOps(B22, temp, B11, S6, size);
	matrixSubU(B22, B12, S7, size);
	negateUboundMatrix(temp, B22, size);
	negateUboundMatrix(temp2, B11, size);
	matrixAddWithThreeFusedOps(B21, temp, B12, temp2, S8, size);
	destroyArrayU(temp);
	destroyArrayU(temp2);
#endif

#ifdef REGULAR_ADDS
		matrixAddU(A21, A22, S1, size);
		matrixSubU(S1, A11, S2, size);
		matrixSubU(A11, A21, S3, size);
		matrixSubU(A12, S2, S4, size);
		matrixSubU(B12, B11, S5, size);
		matrixSubU(B22, S5, S6, size);
		matrixSubU(B22, B12, S7, size);
		matrixSubU(B21, S6, S8, size);
#endif

	//multiplications
	matMulStrassenUBlockingAllFusedRoundRobin(size, A11, B11, M1);
	matMulStrassenUBlockingAllFusedRoundRobin(size, A12, B21, M2);
	matMulStrassenUBlockingAllFusedRoundRobin(size, S1, S5, M3);
	matMulStrassenUBlockingAllFusedRoundRobin(size, S2, S6, M4);
	matMulStrassenUBlockingAllFusedRoundRobin(size, S3, S7, M5);
	matMulStrassenUBlockingAllFusedRoundRobin(size, S4, B22, M6);
	matMulStrassenUBlockingAllFusedRoundRobin(size, A22, S8, M7);

	//additions
	matrixAddU(M1, M2, C11, size);
	matrixAddU(M1, M4, T1, size);
	matrixAddU(T1, M5, T2, size);
	matrixAddU(T2, M7, C21, size);
	matrixAddU(T2, M3, C22, size);
	matrixAddU(T1, M3, T3, size);
	matrixAddU(T3, M6, C12, size);
	if (thisPermutation[8] == 1 && thisPermutation[9] == 2
			&& thisPermutation[10] == 3 && thisPermutation[11] == 4) {
		combineMatricesU(C11, C12, C21, C22, n, C);
	} else if (thisPermutation[8] == 3 && thisPermutation[9] == 4
			&& thisPermutation[10] == 1 && thisPermutation[11] == 2) {
		combineMatricesU(C21, C22, C11, C12, n, C);
	} else if (thisPermutation[8] == 2 && thisPermutation[9] == 1
			&& thisPermutation[10] == 4 && thisPermutation[11] == 3) {
		combineMatricesU(C12, C11, C22, C21, n, C);
	} else {
		combineMatricesU(C22, C21, C12, C11, n, C);
	}

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T1);
	destroyArrayU(T2);
	destroyArrayU(T3);
}

void matMulStrassenUBlockingAllFused(int n, ubound_t** A, ubound_t** B,
		ubound_t** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		//matMulTraditionalU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] = A[i][j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A12[i][j] = A[i][j + size];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A21[i][j] = A[i + size][j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A22[i][j] = A[i + size][j + size];
		}
	}

	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j] = B[i][j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B12[i][j] = B[i][j + size];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B21[i][j] = B[i + size][j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B22[i][j] = B[i + size][j + size];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T1 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);
	ubound_t** temp = createUboundArray(size, size);
	ubound_t** temp2 = createUboundArray(size, size);

	matrixAddU(A21, A22, S1, size);
	matrixSubU(S1, A11, S2, size);
	matrixSubU(A11, A21, S3, size);
	matrixSubU(A12, S2, S4, size);
	matrixSubU(B12, B11, S5, size);
	matrixSubU(B22, S5, S6, size);
	matrixSubU(B22, B12, S7, size);
	matrixSubU(B21, S6, S8, size);

	//multiplications
	matMulStrassenUBlockingAllFused(size, A11, B11, M1);
	matMulStrassenUBlockingAllFused(size, A12, B21, M2);
	matMulStrassenUBlockingAllFused(size, S1, S5, M3);
	matMulStrassenUBlockingAllFused(size, S2, S6, M4);
	matMulStrassenUBlockingAllFused(size, S3, S7, M5);
	matMulStrassenUBlockingAllFused(size, S4, B22, M6);
	matMulStrassenUBlockingAllFused(size, A22, S8, M7);

	//additions
	matrixAddU(M1, M2, C11, size);
	matrixAddU(M1, M4, T1, size);
	matrixAddU(T1, M5, T2, size);
	matrixAddU(T2, M7, C21, size);
	matrixAddU(T2, M3, C22, size);
	matrixAddU(T1, M3, T3, size);
	matrixAddU(T3, M6, C12, size);
	combineMatricesU(C11, C12, C21, C22, n, C);

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T1);
	destroyArrayU(T2);
	destroyArrayU(T3);
}

void matrixAddU(ubound_t** A, ubound_t** B, ubound_t** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			plusubound(&C[i][j], A[i][j], B[i][j]);
		}
	}
	return;
}

void matrixSubU(ubound_t** A, ubound_t** B, ubound_t** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			minusubound(&C[i][j], A[i][j], B[i][j]);
		}
	}
	return;
}

void matrixAddWithTwoFusedOps(ubound_t** A, ubound_t** B, ubound_t** C,
		ubound_t** result, int size) {
	int i, j;
	ubound_t input[3];

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			input[0] = A[i][j];
			input[1] = B[i][j];
			input[2] = C[i][j];
			fused_ubound_add(&result[i][j], 3, input);
		}
	}
	return;
}

void matrixAddWithThreeFusedOps(ubound_t** A, ubound_t** B, ubound_t** C,
		ubound_t** D, ubound_t** result, int size) {
	int i, j;
	ubound_t input[4];

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			input[0] = A[i][j];
			input[1] = B[i][j];
			input[2] = C[i][j];
			input[3] = D[i][j];
			fused_ubound_add(&result[i][j], 4, input);
		}
	}
	return;
}

void combineMatricesU(ubound_t** C11, ubound_t** C12, ubound_t** C21,
		ubound_t** C22, int n, ubound_t** result) {
	int size = n / 2, i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i][j] = C11[i][j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i][j + size] = C12[i][j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i + size][j] = C21[i][j];
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i + size][j + size] = C22[i][j];
		}
	}
	return;
}

void negateUboundMatrix(ubound_t** negated, ubound_t** mat, int size) {
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			negated[i][j].left_bound = mat[i][j].right_bound;
			negated[i][j].right_bound = mat[i][j].left_bound;
			negated[i][j].left_bound.sign = !negated[i][j].left_bound.sign;
			negated[i][j].right_bound.sign = !negated[i][j].right_bound.sign;
		}
	}
}

void matMulTraditionalFusedU(int n, ubound_t** A, ubound_t** B, ubound_t** C) {
	int i, j, k;
	ubound_t temp_a[n], temp_b[n];

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {

			for (k = 0; k < n; k++) {
				temp_a[k] = A[i][k];
				temp_b[k] = B[k][j];
			}
			fused_ubound_dot_product(&C[i][j], n, temp_a, temp_b);
		}
	}
}

void matMulTraditionalU(int n, ubound_t** A, ubound_t** B, ubound_t** C) {
	int i, j, k;
	ubound_t temp;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			C[i][j].left_bound = _g_zero;
			C[i][j].right_bound = _g_zero;
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				timesubound(&temp, A[i][k], B[k][j]);
				plusubound(&C[i][j], C[i][j], temp);
			}
		}
	}
}

ubound_t** createUboundArray(int rows, int columns) {
	ubound_t* values = calloc(rows * columns, sizeof(ubound_t));
	ubound_t** valueRows = malloc(columns * sizeof(ubound_t*));
	for (int i = 0; i < columns; ++i) {
		valueRows[i] = values + i * rows;
	}
	return valueRows;
}

double** createDoubleArray(int rows, int columns) {
	double* values = calloc(rows * columns, sizeof(double));
	double** valueRows = malloc(columns * sizeof(double*));
	for (int i = 0; i < columns; ++i) {
		valueRows[i] = values + i * rows;
	}
	return valueRows;
}

void assignRandomDoubleMatrix(double** matrix, int rows, int columns) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; j++) {
			matrix[i][j] = -1 + 2 * ((double) rand()) / RAND_MAX;
			//matrix[i][j] = (double) rand() / (double) RAND_MAX;
		}
	}
	return;
}

mpfr_t** createMPFRArray(int rows, int columns) {
	int size = rows * columns, i;
	mpfr_t *num_arr;
	num_arr = calloc(size, sizeof(mpfr_t));
	for (i = 0; i < size; i++) {
		mpfr_init2(num_arr[i], MPFR_PREC);
	}
	mpfr_t** valueRows = malloc(columns * sizeof(mpfr_t*));
	for (int i = 0; i < columns; ++i) {
		valueRows[i] = num_arr + i * rows;
	}
	return valueRows;
}

void assignDoubleToMPFRMatrix(double** matrixd, mpfr_t** matrixMPFR, int rows,
		int columns) {
	int i, j;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < columns; j++) {
			mpfr_set_d(matrixMPFR[i][j], matrixd[i][j], MPFR_ROUNDING_MODE);
		}
	}
}

void assignMPFRToUboundMatrix(mpfr_t** matrixmpfr, ubound_t** matrixU, int rows,
		int columns) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; ++j) {
			x2ub(mpfr_get_d(matrixmpfr[i][j], MPFR_ROUNDING_MODE),
					&matrixU[i][j]);
		}
	}
}

void matMulTraditionalDouble(int n, double** A, double** B, double** C) {
	int i, j, k;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			C[i][j] = 0;
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
}

void printToFile(double **error, char* file_name) {
	char buffer[100];
	FILE *f;
	int i, j, k;
	double max, min, sum, sum_dev, mean;

	snprintf(buffer, sizeof(char) * 100, "%dx%d/%s", MAT_SIZE,
			MAT_SIZE, file_name);
	f = fopen(buffer, "w");
	fprintf(f, "\n%s\n", file_name);
	for (j = 0; j < MAT_SIZE; j++) {
		//fprintf(f, "{");
		for (k = 0; k < MAT_SIZE; k++) {
			max = 0.0, min = 1.0;
			sum_dev = 0.0;
			sum = 0.0;
			for (i = 0; i < REPEAT; i++) {
				sum += fabs(error[i][(j * MAT_SIZE) + k]);
				if (fabs(error[i][(j * MAT_SIZE) + k]) < fabs(min))
					min = error[i][(j * MAT_SIZE) + k];

				if (fabs(error[i][(j * MAT_SIZE) + k]) > fabs(max))
					max = error[i][(j * MAT_SIZE) + k];
			}
			mean = sum / REPEAT;
			for (i = 0; i < REPEAT; ++i)
				sum_dev += (fabs(error[i][(j * MAT_SIZE) + k]) - mean)
						* (fabs(error[i][(j * MAT_SIZE) + k]) - mean);
			/*fprintf(f,
			 "%d %d min = %e, max = %e, mean = %e, standard deviation = %e\n",
			 j, k, min, max, mean, sum_dev);*/
			/*if (k == (mat_size - 1))
			 fprintf(f, "%e", mean);
			 else*/
			fprintf(f, "%e, ", mean);
		}
		//fprintf(f, "}, ");
	}
	fclose(f);
}

void destroyArrayU(ubound_t** arr) {
	free(*arr);
	free(arr);
}

double getUboundWidth(ubound_t ub) {
	gbound_t gb;
	double dleft, dright, result;

	get_gbound_from_ubound(&gb, &ub);

	dleft = u2f(gb.left_bound);
	dright = u2f(gb.right_bound);
	result = -log10((log10(dright / dleft)));

	return result;
}

void destroyArrayMPFR(mpfr_t** arr, int size) {
	int i, j;
	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			mpfr_clear(arr[i][j]);
		}
	}
	free(*arr);
	free(arr);
}

void destroyArrayD(double** arr) {
	free(*arr);
	free(arr);
}

void printMatrixU(size_t n, ubound_t** mat, char* mat_name) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%s[%d][%d]=\n", mat_name, i, j);
			uboundview(mat[i][j]);
		}
	}
}

void printWidthMatrixU(size_t n, ubound_t** mat, char* mat_name) {
	int i, j;
	printf("%s\tsize = %lu\n", mat_name, n);
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%e\t", getUboundWidth(mat[i][j]));
		}
		printf("\n");
	}
	printf("\n");
}

