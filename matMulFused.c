#include <stdio.h>
#include <libunum.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <float.h>
#include <gmp.h>
#include <mpfr.h>
#include <sys/stat.h>
#include <sys/types.h>

#define IS_POWER_OF_2(n) ((n != 0) && ((n & (n - 1)) == 0))
#define MAT_SIZE 256
#define REPEAT 1
#define BINS 8000
#define UNUMBINS 10000
#define MPFR_ROUNDING_MODE MPFR_RNDN
#define BLOCK MAT_SIZE/4
#define FUSE_LENGTH MAT_SIZE/2
#define ESIZESIZE 3
#define FSIZESIZE 5
#define MPFR_PREC 23

int permuatations[8][12] = { { 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4 }, { 2, 1, 4,
		3, 3, 4, 1, 2, 1, 2, 3, 4 }, { 3, 4, 1, 2, 1, 2, 3, 4, 3, 4, 1, 2 }, {
		4, 3, 2, 1, 3, 4, 1, 2, 3, 4, 1, 2 }, { 1, 2, 3, 4, 2, 1, 4, 3, 2, 1, 4,
		3 }, { 2, 1, 4, 3, 4, 3, 2, 1, 2, 1, 4, 3 }, { 3, 4, 1, 2, 2, 1, 4, 3,
		4, 3, 2, 1 }, { 4, 3, 2, 1, 4, 3, 2, 1, 4, 3, 2, 1 } };
int currentPermutationIndex = -1;

float** createFloatArray(int rows, int columns);
double** createDoubleArray(int rows, int columns);
ubound_t** createUboundArray(int rows, int columns);
mpfr_t** createMPFRArray(int rows, int columns);
mpfr_t** createMPFRExactArray(int rows, int columns);
void assignRandomDoubleMatrix(double** matrix, int rows, int columns);
void assignDoubleToFloatMatrix(double** matrixd, float** matrixf, int rows,
		int columns);
void assignDoubleToUboundMatrix(double** matrixd, ubound_t** matrixU, int rows,
		int columns);
void assignMPFRToUboundMatrix(mpfr_t** matrixmpfr, ubound_t** matrixU, int rows,
		int columns);
void assignDoubleToMPFRMatrix(double** matrixd, mpfr_t** matrixMPFR, int rows,
		int columns);
void matMulTraditionalFusedU(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulTraditionalDouble(int n, double** A, double** B, double** C);
void matMulTraditionalFloat(int n, float** A, float** B, float** C);
void matMulTraditionalMPFR(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C);
void matMulTraditionalU(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulStrassenUBlocking(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulStrassenUFusedDP(int n, ubound_t** A, ubound_t** B, ubound_t** C);
void matMulStrassenUBlockingAllFused(int n, ubound_t** A, ubound_t** B,
		ubound_t** C);
void matrixAddU(ubound_t** A, ubound_t** B, ubound_t** C, int size);
void matrixSubU(ubound_t** A, ubound_t** B, ubound_t** C, int size);
void combineMatricesU(ubound_t** C11, ubound_t** C12, ubound_t** C21,
		ubound_t** C22, int n, ubound_t** result);
void destroyArray(float** arr);
void destroyArrayU(ubound_t** arr);
void destroyArrayD(double** arr);
void destroyArrayMPFR(mpfr_t** arr, int size);
void do4x4MatMul(int mat_size);
double getUboundWidth(ubound_t ub);
void printMatrix(size_t n, float** mat);
void printMatrixDouble(size_t n, double** mat);
void printMatrixU(size_t n, ubound_t** mat, char* mat_name);
void printMatrixMPFR(size_t n, mpfr_t** mat);
void matrixAddWithTwoFusedOps(ubound_t** A, ubound_t** B, ubound_t** C,
		ubound_t** result, int size);
void negateUboundMatrix(ubound_t** negated, ubound_t** mat, int size);
void matrixAddWithThreeFusedOps(ubound_t** A, ubound_t** B, ubound_t** C,
		ubound_t** D, ubound_t** result, int size);
void printToFile(double **error, char* file_name);
void matMulTraditionalFusedVarFLU(int n, ubound_t** A, ubound_t** B,
		ubound_t** C);
void matMulStrassenUBlockingAllFusedVFL(int n, ubound_t** A, ubound_t** B,
		ubound_t** C);
void printErrorMatrix(int rows, int columns, double** mat);
void printWidthMatrixU(size_t n, ubound_t** mat, char* mat_name);
void fusedDPMPFR(int n, mpfr_t* A, mpfr_t* B, mpfr_t result);
void matMulTraditionalMPFRFDP(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C);
void matMulStrassenMPFR(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C);
void matMulStrassenMPFRFusedDP(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C);
void matrixAddMPFR(mpfr_t** A, mpfr_t** B, mpfr_t** C, int size);
void matrixSubMPFR(mpfr_t** A, mpfr_t** B, mpfr_t** C, int size);
void combineMatricesMPFR(mpfr_t** C11, mpfr_t** C12, mpfr_t** C21, mpfr_t** C22,
		int n, mpfr_t** result);
void matMulTraditionalMPFRExact(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C);

int main() {
	srand(time(0));
	do4x4MatMul(MAT_SIZE);
}

void do4x4MatMul(int mat_size) {
	set_env(ESIZESIZE, FSIZESIZE);
	_g_fsizemax = MPFR_PREC;

	int i, j, k;
	char buffer[100];

	double **d1, **d2, **dresult;
	float **f1, **f2, **fresult;
	ubound_t **u1, **u2, **uTradResult, **uStrassenResult,
			**uStrassenFusedResult, **uTradFusedResult, **uStrassenFusedDP;
	mpfr_t **mpfr1, **mpfr2, **mpfrTradResult, **mpfrStrasResult,
			**mpfrFusedDPStrasResult, **mpfrTradFusedResult, **mpfrTradExactResult;

	double **eTradfloat, **eTradUnum, **eStrassenUnum, **eStrassenFusedUnum,
			**eTradMPFR, **eTradUnumFused, **eStrassenFusedDP, **eStrassenMPFR,
			**eStrassenMPFRFusedDP, **eTradMPFRFused;

#ifdef VAR_FL
	ubound_t **uTradVarFusedLengthResult, **uStrasVarFusedLengthResult;
	double **eTradUnumFusedVFL, **eStrasUnumFusedVFL;
#endif

	if (mat_size == 0)
		mat_size = 4;

	printf("Matrix size = %d\n", mat_size);
	printf("Block size = %d\n", BLOCK);
	printf("REPEAT = %d\n", REPEAT);

	// initialize all matrices
	eTradfloat = createDoubleArray(REPEAT, (mat_size * mat_size));
	eTradUnum = createDoubleArray(REPEAT, (mat_size * mat_size));
	eStrassenUnum = createDoubleArray(REPEAT, (mat_size * mat_size));
	eStrassenFusedUnum = createDoubleArray(REPEAT, (mat_size * mat_size));
	eTradMPFR = createDoubleArray(REPEAT, (mat_size * mat_size));
	eStrassenMPFR = createDoubleArray(REPEAT, (mat_size * mat_size));
	eStrassenMPFRFusedDP = createDoubleArray(REPEAT, (mat_size * mat_size));
	eTradUnumFused = createDoubleArray(REPEAT, (mat_size * mat_size));
	eStrassenFusedDP = createDoubleArray(REPEAT, (mat_size * mat_size));
	eTradMPFRFused = createDoubleArray(REPEAT, (mat_size * mat_size));

#ifdef VAR_FL
	eTradUnumFusedVFL = createDoubleArray(REPEAT, (mat_size * mat_size));
	eStrasUnumFusedVFL = createDoubleArray(REPEAT, (mat_size * mat_size));
#endif

	d1 = createDoubleArray(mat_size, mat_size);
	d2 = createDoubleArray(mat_size, mat_size);
	dresult = createDoubleArray(mat_size, mat_size);

	f1 = createFloatArray(mat_size, mat_size);
	f2 = createFloatArray(mat_size, mat_size);
	fresult = createFloatArray(mat_size, mat_size);

	u1 = createUboundArray(mat_size, mat_size);
	u2 = createUboundArray(mat_size, mat_size);
	uTradResult = createUboundArray(mat_size, mat_size);
	uStrassenResult = createUboundArray(mat_size, mat_size);
	uStrassenFusedResult = createUboundArray(mat_size, mat_size);
	uTradFusedResult = createUboundArray(mat_size, mat_size);
	uStrassenFusedDP = createUboundArray(mat_size, mat_size);

#ifdef VAR_FL
	uTradVarFusedLengthResult = createUboundArray(mat_size, mat_size);
	uStrasVarFusedLengthResult = createUboundArray(mat_size, mat_size);
#endif

	mpfr1 = createMPFRArray(mat_size, mat_size);
	mpfr2 = createMPFRArray(mat_size, mat_size);
	mpfrTradResult = createMPFRArray(mat_size, mat_size);
	mpfrStrasResult = createMPFRArray(mat_size, mat_size);
	mpfrFusedDPStrasResult = createMPFRArray(mat_size, mat_size);
	mpfrTradFusedResult = createMPFRArray(mat_size, mat_size);
	mpfrTradExactResult = createMPFRExactArray(mat_size, mat_size);

	//compute
	for (i = 0; i < REPEAT; i++) {
		printf("i = %d\n", i);

		assignRandomDoubleMatrix(d1, mat_size, mat_size);
		assignRandomDoubleMatrix(d2, mat_size, mat_size);

		assignDoubleToFloatMatrix(d1, f1, mat_size, mat_size);
		assignDoubleToFloatMatrix(d2, f2, mat_size, mat_size);

		/*assignDoubleToUboundMatrix(d1, u1, mat_size, mat_size);
		assignDoubleToUboundMatrix(d2, u2, mat_size, mat_size);*/

		assignDoubleToMPFRMatrix(d1, mpfr1, mat_size, mat_size);
		assignDoubleToMPFRMatrix(d2, mpfr2, mat_size, mat_size);

		assignMPFRToUboundMatrix(mpfr1, u1, mat_size, mat_size);
		assignMPFRToUboundMatrix(mpfr2, u2, mat_size, mat_size);

		matMulTraditionalDouble(mat_size, d1, d2, dresult);
		/*matMulTraditionalFloat(mat_size, f1, f2, fresult);
		matMulStrassenUBlocking(mat_size, u1, u2, uStrassenResult);
		matMulStrassenUBlockingAllFused(mat_size, u1, u2, uStrassenFusedResult);
		matMulTraditionalMPFRFDP(mat_size, mpfr1, mpfr2, mpfrTradFusedResult);
		matMulStrassenMPFR(mat_size, mpfr1, mpfr2, mpfrStrasResult);
		*/

#ifdef VAR_FL
		matMulStrassenUBlockingAllFusedVFL(mat_size, u1, u2, uStrasVarFusedLengthResult);
		matMulTraditionalFusedVarFLU(mat_size, u1, u2, uTradVarFusedLengthResult);
#endif

		matMulTraditionalMPFRExact(mat_size, mpfr1, mpfr2, mpfrTradExactResult);
		matMulTraditionalMPFR(mat_size, mpfr1, mpfr2, mpfrTradResult);
		matMulStrassenMPFRFusedDP(mat_size, mpfr1, mpfr2, mpfrFusedDPStrasResult);
		matMulTraditionalU(mat_size, u1, u2, uTradResult);
		matMulTraditionalFusedU(mat_size, u1, u2, uTradFusedResult);
		matMulStrassenUFusedDP(mat_size, u1, u2, uStrassenFusedDP);

		/*printMatrixDouble(mat_size, dresult);
		printf("\nmpfr stras 4\n");
		printMatrixMPFR(mat_size, mpfrStrasResult);
		printf("\nmpfr 2\n");
		printMatrixMPFR(mat_size, mpfrFusedDPStrasResult);
		printf("u 5\n");
		printMatrixU(mat_size, uTradResult, "5");
		printf("u 1\n");
		printMatrixU(mat_size, uTradFusedResult, "1");
		printf("u 3\n");
		printMatrixU(mat_size, uStrassenFusedDP, "3");

		printMatrix(MAT_SIZE, fresult);
		 printMatrixU(MAT_SIZE, uTradResult, "stras ubound trad");
		 printMatrixU(MAT_SIZE, uStrassenResult, "stas ubound");
		 printMatrixU(MAT_SIZE, uStrassenFusedResult, "stas ubound fused");*/

		/*for (j = 0; j < mat_size; j++) {
		 for (k = 0; k < mat_size; k++) {
		 index = (j * mat_size) + k;
		 eTradfloat[i][index] = dresult[j][k] - fresult[j][k];
		 }
		 }*/

		for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				eTradUnum[i][(j * mat_size) + k] = getUboundWidth(uTradResult[j][k]);
				eStrassenUnum[i][(j * mat_size) + k] = getUboundWidth(uStrassenResult[j][k]);
				eStrassenFusedUnum[i][(j * mat_size) + k] = getUboundWidth(uStrassenFusedResult[j][k]);
				eTradUnumFused[i][(j * mat_size) + k] = getUboundWidth(uTradFusedResult[j][k]);
				eTradMPFR[i][(j * mat_size) + k] = fabs(log10(dresult[j][k] / mpfr_get_d(mpfrTradResult[j][k], MPFR_ROUNDING_MODE)));

				mpfr_div(mpfrTradExactResult[j][k], mpfrTradExactResult[j][k], mpfrTradFusedResult[j][k], MPFR_ROUNDING_MODE);
				eTradMPFRFused[i][(j * mat_size) + k] = fabs(log10(mpfr_get_d(mpfrTradExactResult[j][k], MPFR_ROUNDING_MODE)));

				eStrassenMPFR[i][(j * mat_size) + k] = fabs(log10(dresult[j][k] / mpfr_get_d(mpfrStrasResult[j][k], MPFR_ROUNDING_MODE)));
				eStrassenMPFRFusedDP[i][(j * mat_size) + k] = fabs(log10(dresult[j][k] / mpfr_get_d(mpfrFusedDPStrasResult[j][k], MPFR_ROUNDING_MODE)));
				eStrassenFusedDP[i][(j * mat_size) + k] = getUboundWidth(uStrassenFusedDP[j][k]);
			}
		}

#ifdef VAR_FL
		for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				eTradUnumFusedVFL[i][(j * mat_size) + k] = getUboundWidth(
						uTradVarFusedLengthResult[j][k]);
			}
		}

		for (j = 0; j < mat_size; j++) {
			for (k = 0; k < mat_size; k++) {
				eStrasUnumFusedVFL[i][(j * mat_size) + k] = getUboundWidth(
						uStrasVarFusedLengthResult[j][k]);
			}
		}
#endif
	}

	destroyArray(f1);
	destroyArray(f2);
	destroyArray(fresult);
	destroyArrayD(d1);
	destroyArrayD(d2);
	destroyArrayD(dresult);
	destroyArrayU(u1);
	destroyArrayU(u2);
	destroyArrayU(uTradResult);
	destroyArrayU(uStrassenResult);
	destroyArrayU(uTradFusedResult);
	destroyArrayMPFR(mpfr1, mat_size);
	destroyArrayMPFR(mpfr2, mat_size);
	destroyArrayMPFR(mpfrTradResult, mat_size);
#ifdef VAR_FL
	destroyArrayU(uTradVarFusedLengthResult);
	destroyArrayU(uStrasVarFusedLengthResult);
#endif

	snprintf(buffer, sizeof(char) * 100, "%dx%d", mat_size, mat_size);
	mkdir(buffer, 0700);
	//6.
	printToFile(eTradMPFRFused, "6_trad_mpfr_fused.txt");

	//4. Traditional statistical without fused dot product
	printToFile(eTradMPFR, "4_trad_mpfr.txt");

	//2. Strassen statistical with fused dot product
	printToFile(eStrassenMPFRFusedDP, "2_Strassen_mpfr_fused.txt");

	//5. worst case traditional (without fused dot product)
	printToFile(eTradUnum, "5_trad_unum.txt");

	//1. Traditional with fused DP - best case
	printToFile(eTradUnumFused, "1_trad_unum_fused.txt");

	//3. Strassen worst case with fused dot product
	printToFile(eStrassenFusedDP, "3_Strassen_fused_DP_unum.txt");

	/*printToFile(eStrassenUnum, "Strassen_unum.txt");
	 printToFile(eStrassenFusedUnum, "Strassen_fused_unum.txt");*/

#ifdef VAR_FL
	printToFile(eTradUnumFusedVFL, "trad_unum_fused_vfl.txt");
	printToFile(eStrasUnumFusedVFL, "Strassen_unum_fused_vfl.txt");
#endif

	destroyArrayD(eTradfloat);
	destroyArrayD(eTradUnum);
	destroyArrayD(eStrassenUnum);
	destroyArrayD(eTradMPFR);
	destroyArrayD(eTradUnumFused);
	destroyArrayD(eStrassenFusedDP);
#ifdef VAR_FL
	destroyArrayD(eTradUnumFusedVFL);
	destroyArrayD(eStrasUnumFusedVFL);
#endif

	printf("Done!");
}

void matMulTraditionalU(int n, ubound_t** A, ubound_t** B, ubound_t** C) {
	int i, j, k;
	ubound_t temp;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			C[i][j].left_bound = _g_zero;
			C[i][j].right_bound = _g_zero;
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				timesubound(&temp, A[i][k], B[k][j]);
				plusubound(&C[i][j], C[i][j], temp);
			}
		}
	}
}

void matMulTraditionalFusedU(int n, ubound_t** A, ubound_t** B, ubound_t** C) {
	int i, j, k;
	ubound_t temp_a[n], temp_b[n];

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {

			for (k = 0; k < n; k++) {
				temp_a[k] = A[i][k];
				temp_b[k] = B[k][j];
			}
			fused_ubound_dot_product(&C[i][j], n, temp_a, temp_b);
		}
	}
}

void matMulTraditionalFusedVarFLU(int n, ubound_t** A, ubound_t** B,
		ubound_t** C) {
	int i, j, k, l;
	ubound_t temp_a[FUSE_LENGTH], temp_b[FUSE_LENGTH], temp_sum;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			C[i][j].left_bound = _g_zero;
			C[i][j].right_bound = _g_zero;

			for (k = 0; k < n; k = k + FUSE_LENGTH) {
				for (l = 0; l < FUSE_LENGTH; l++) {
					temp_a[l] = A[i][k + l];
					temp_b[l] = B[k + l][j];
				}
				fused_ubound_dot_product(&temp_sum, FUSE_LENGTH, temp_a,
						temp_b);
				plusubound(&C[i][j], C[i][j], temp_sum);
			}

			l = n % FUSE_LENGTH;
			if (l > 0 && n != FUSE_LENGTH) {
				for (k = 0; k < l; k++) {
					temp_a[k] = A[i][n - 1 - k];
					temp_b[k] = B[n - 1 - k][j];
				}
				fused_ubound_dot_product(&temp_sum, l, temp_a, temp_b);
				plusubound(&C[i][j], C[i][j], temp_sum);
			}
		}
	}
}

void matMulStrassenUBlockingAllFused(int n, ubound_t** A, ubound_t** B,
		ubound_t** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);
	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] = A[i][j];
			A12[i][j] = A[i][j + size];
			A21[i][j] = A[i + size][j];
			A22[i][j] = A[i + size][j + size];

			B11[i][j] = B[i][j];
			B12[i][j] = B[i][j + size];
			B21[i][j] = B[i + size][j];
			B22[i][j] = B[i + size][j + size];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** T6 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);
	ubound_t** temp = createUboundArray(size, size);
	ubound_t** temp2 = createUboundArray(size, size);

	//additions
	matrixAddU(A21, A22, S1, size);
	negateUboundMatrix(temp, A11, size);
	matrixAddWithTwoFusedOps(A21, A22, temp, S2, size);
	matrixSubU(A11, A21, S3, size);
	negateUboundMatrix(temp, A21, size);
	negateUboundMatrix(temp2, A22, size);
	matrixAddWithThreeFusedOps(A12, temp, temp2, A11, S4, size);
	matrixSubU(B12, B11, S5, size);
	negateUboundMatrix(temp, B12, size);
	matrixAddWithTwoFusedOps(B22, temp, B11, S6, size);
	matrixSubU(B22, B12, S7, size);
	negateUboundMatrix(temp, B22, size);
	negateUboundMatrix(temp2, B11, size);
	matrixAddWithThreeFusedOps(B21, temp, B12, temp2, S8, size);
	destroyArrayU(temp);
	destroyArrayU(temp2);

	//multiplications
	matMulStrassenUBlockingAllFused(size, A11, B11, M1);
	matMulStrassenUBlockingAllFused(size, A12, B21, M2);
	matMulStrassenUBlockingAllFused(size, S1, S5, M3);
	matMulStrassenUBlockingAllFused(size, S2, S6, M4);
	matMulStrassenUBlockingAllFused(size, S3, S7, M5);
	matMulStrassenUBlockingAllFused(size, S4, B22, M6);
	matMulStrassenUBlockingAllFused(size, A22, S8, M7);

	//additions
	matrixAddU(M1, M2, C11, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M7, C21, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M3, C22, size);
	matrixAddWithThreeFusedOps(M1, M4, M3, M6, C12, size);
	combineMatricesU(C11, C12, C21, C22, n, C);

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T2);
	destroyArrayU(T3);
	destroyArrayU(T6);
}

void matMulStrassenUFusedDP(int n, ubound_t** A, ubound_t** B, ubound_t** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] = A[i][j];
			A12[i][j] = A[i][j + size];
			A21[i][j] = A[i + size][j];
			A22[i][j] = A[i + size][j + size];
		}
	}

	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j] = B[i][j];
			B12[i][j] = B[i][j + size];
			B21[i][j] = B[i + size][j];
			B22[i][j] = B[i + size][j + size];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** T6 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);

	matrixAddU(A21, A22, S1, size);
	matrixSubU(S1, A11, S2, size);
	matrixSubU(A11, A21, S3, size);
	matrixSubU(A12, S2, S4, size);
	matrixSubU(B12, B11, S5, size);
	matrixSubU(B22, S5, S6, size);
	matrixSubU(B22, B12, S7, size);
	matrixSubU(B21, S6, S8, size);
	matMulStrassenUFusedDP(size, A11, B11, M1);
	matMulStrassenUFusedDP(size, A12, B21, M2);
	matMulStrassenUFusedDP(size, S1, S5, M3);
	matMulStrassenUFusedDP(size, S2, S6, M4);
	matMulStrassenUFusedDP(size, S3, S7, M5);
	matMulStrassenUFusedDP(size, S4, B22, M6);
	matMulStrassenUFusedDP(size, A22, S8, M7);
	matrixAddU(M1, M2, C11, size);
	matrixAddU(M1, M4, T2, size);
	matrixAddU(T2, M5, T3, size);
	matrixAddU(T3, M7, C21, size);
	matrixAddU(T3, M3, C22, size);
	matrixAddU(T2, M3, T6, size);
	matrixAddU(T6, M6, C12, size);
	combineMatricesU(C11, C12, C21, C22, n, C);

	/*printWidthMatrixU(size, A11, "A11");
	 printWidthMatrixU(size, B11, "B11");
	 printWidthMatrixU(size, M1, "M1");

	 printMatrixU(size, A11, "A11");
	 printMatrixU(size, B11, "B11");*/

	/*printWidthMatrixU(size, A12, "A12");
	 printWidthMatrixU(size, B21, "B21");
	 printWidthMatrixU(size, M2, "M2");

	 printWidthMatrixU(size, S1, "S1");
	 printWidthMatrixU(size, S5, "S5");
	 printWidthMatrixU(size, M3, "M3");

	 printWidthMatrixU(size, S2, "S2");
	 printWidthMatrixU(size, S6, "S6");
	 printWidthMatrixU(size, M4, "M4");

	 printWidthMatrixU(size, S3, "S3");
	 printWidthMatrixU(size, S7, "S7");
	 printWidthMatrixU(size, M5, "M5");

	 printWidthMatrixU(size, B22, "B22");
	 printWidthMatrixU(size, S4, "S4");
	 printWidthMatrixU(size, M6, "M6");

	 printWidthMatrixU(size, A22, "A22");
	 printWidthMatrixU(size, S8, "S8");
	 printWidthMatrixU(size, M7, "M7");*/

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T2);
	destroyArrayU(T3);
	destroyArrayU(T6);

}

void matMulStrassenUBlockingAllFusedVFL(int n, ubound_t** A, ubound_t** B,
		ubound_t** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalFusedVarFLU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] = A[i][j];
			A12[i][j] = A[i][j + size];
			A21[i][j] = A[i + size][j];
			A22[i][j] = A[i + size][j + size];
		}
	}

	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j] = B[i][j];
			B12[i][j] = B[i][j + size];
			B21[i][j] = B[i + size][j];
			B22[i][j] = B[i + size][j + size];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** T6 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);
	ubound_t** temp = createUboundArray(size, size);
	ubound_t** temp2 = createUboundArray(size, size);

	//additions
	matrixAddU(A21, A22, S1, size);
	negateUboundMatrix(temp, A11, size);
	matrixAddWithTwoFusedOps(A21, A22, temp, S2, size);
	matrixSubU(A11, A21, S3, size);
	negateUboundMatrix(temp, A21, size);
	negateUboundMatrix(temp2, A22, size);
	matrixAddWithThreeFusedOps(A12, temp, temp2, A11, S4, size);
	matrixSubU(B12, B11, S5, size);
	negateUboundMatrix(temp, B12, size);
	matrixAddWithTwoFusedOps(B22, temp, B11, S6, size);
	matrixSubU(B22, B12, S7, size);
	negateUboundMatrix(temp, B22, size);
	negateUboundMatrix(temp2, B11, size);
	matrixAddWithThreeFusedOps(B21, temp, B12, temp2, S8, size);
	destroyArrayU(temp);
	destroyArrayU(temp2);

	//multiplications
	matMulStrassenUBlockingAllFusedVFL(size, A11, B11, M1);
	matMulStrassenUBlockingAllFusedVFL(size, A12, B21, M2);
	matMulStrassenUBlockingAllFusedVFL(size, S1, S5, M3);
	matMulStrassenUBlockingAllFusedVFL(size, S2, S6, M4);
	matMulStrassenUBlockingAllFusedVFL(size, S3, S7, M5);
	matMulStrassenUBlockingAllFusedVFL(size, S4, B22, M6);
	matMulStrassenUBlockingAllFusedVFL(size, A22, S8, M7);

	//additions
	matrixAddU(M1, M2, C11, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M7, C21, size);
	matrixAddWithThreeFusedOps(M1, M4, M5, M3, C22, size);
	matrixAddWithThreeFusedOps(M1, M4, M3, M6, C12, size);
	combineMatricesU(C11, C12, C21, C22, n, C);

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T2);
	destroyArrayU(T3);
	destroyArrayU(T6);
}

void negateUboundMatrix(ubound_t** negated, ubound_t** mat, int size) {
	int i, j;

	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			negated[i][j].left_bound = mat[i][j].right_bound;
			negated[i][j].right_bound = mat[i][j].left_bound;
			negated[i][j].left_bound.sign = !negated[i][j].left_bound.sign;
			negated[i][j].right_bound.sign = !negated[i][j].right_bound.sign;
		}
	}
}

void matrixAddWithTwoFusedOps(ubound_t** A, ubound_t** B, ubound_t** C,
		ubound_t** result, int size) {
	int i, j;
	ubound_t input[3];

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			input[0] = A[i][j];
			input[1] = B[i][j];
			input[2] = C[i][j];
			fused_ubound_add(&result[i][j], 3, input);
		}
	}
	return;
}

void matrixAddWithThreeFusedOps(ubound_t** A, ubound_t** B, ubound_t** C,
		ubound_t** D, ubound_t** result, int size) {
	int i, j;
	ubound_t input[4];

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			input[0] = A[i][j];
			input[1] = B[i][j];
			input[2] = C[i][j];
			input[3] = D[i][j];
			fused_ubound_add(&result[i][j], 4, input);
		}
	}
	return;
}

void matMulStrassenUBlocking(int n, ubound_t** A, ubound_t** B, ubound_t** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalU(n, A, B, C);
		return;
	}

	int size = n >> 1;

	ubound_t** A11 = createUboundArray(size, size);
	ubound_t** A12 = createUboundArray(size, size);
	ubound_t** A21 = createUboundArray(size, size);
	ubound_t** A22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			A11[i][j] = A[i][j];
			A12[i][j] = A[i][j + size];
			A21[i][j] = A[i + size][j];
			A22[i][j] = A[i + size][j + size];
		}
	}

	ubound_t** B11 = createUboundArray(size, size);
	ubound_t** B12 = createUboundArray(size, size);
	ubound_t** B21 = createUboundArray(size, size);
	ubound_t** B22 = createUboundArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			B11[i][j] = B[i][j];
			B12[i][j] = B[i][j + size];
			B21[i][j] = B[i + size][j];
			B22[i][j] = B[i + size][j + size];
		}
	}

	ubound_t** S1 = createUboundArray(size, size);
	ubound_t** S2 = createUboundArray(size, size);
	ubound_t** S3 = createUboundArray(size, size);
	ubound_t** S4 = createUboundArray(size, size);
	ubound_t** S5 = createUboundArray(size, size);
	ubound_t** S6 = createUboundArray(size, size);
	ubound_t** S7 = createUboundArray(size, size);
	ubound_t** S8 = createUboundArray(size, size);
	ubound_t** M1 = createUboundArray(size, size);
	ubound_t** M2 = createUboundArray(size, size);
	ubound_t** M3 = createUboundArray(size, size);
	ubound_t** M4 = createUboundArray(size, size);
	ubound_t** M5 = createUboundArray(size, size);
	ubound_t** M6 = createUboundArray(size, size);
	ubound_t** M7 = createUboundArray(size, size);
	ubound_t** T2 = createUboundArray(size, size);
	ubound_t** T3 = createUboundArray(size, size);
	ubound_t** T6 = createUboundArray(size, size);
	ubound_t** C11 = createUboundArray(size, size);
	ubound_t** C12 = createUboundArray(size, size);
	ubound_t** C21 = createUboundArray(size, size);
	ubound_t** C22 = createUboundArray(size, size);

	matrixAddU(A21, A22, S1, size);
	matrixSubU(S1, A11, S2, size);
	matrixSubU(A11, A21, S3, size);
	matrixSubU(A12, S2, S4, size);
	matrixSubU(B12, B11, S5, size);
	matrixSubU(B22, S5, S6, size);
	matrixSubU(B22, B12, S7, size);
	matrixSubU(B21, S6, S8, size);
	matMulStrassenUBlocking(size, A11, B11, M1);
	matMulStrassenUBlocking(size, A12, B21, M2);
	matMulStrassenUBlocking(size, S1, S5, M3);
	matMulStrassenUBlocking(size, S2, S6, M4);
	matMulStrassenUBlocking(size, S3, S7, M5);
	matMulStrassenUBlocking(size, S4, B22, M6);
	matMulStrassenUBlocking(size, A22, S8, M7);
	matrixAddU(M1, M2, C11, size);
	matrixAddU(M1, M4, T2, size);
	matrixAddU(T2, M5, T3, size);
	matrixAddU(T3, M7, C21, size);
	matrixAddU(T3, M3, C22, size);
	matrixAddU(T2, M3, T6, size);
	matrixAddU(T6, M6, C12, size);
	combineMatricesU(C11, C12, C21, C22, n, C);

	destroyArrayU(A11);
	destroyArrayU(A12);
	destroyArrayU(A21);
	destroyArrayU(A22);
	destroyArrayU(B11);
	destroyArrayU(B12);
	destroyArrayU(B21);
	destroyArrayU(B22);
	destroyArrayU(C11);
	destroyArrayU(C12);
	destroyArrayU(C21);
	destroyArrayU(C22);
	destroyArrayU(S1);
	destroyArrayU(S2);
	destroyArrayU(S3);
	destroyArrayU(S4);
	destroyArrayU(S5);
	destroyArrayU(S6);
	destroyArrayU(S7);
	destroyArrayU(S8);
	destroyArrayU(M1);
	destroyArrayU(M2);
	destroyArrayU(M3);
	destroyArrayU(M4);
	destroyArrayU(M5);
	destroyArrayU(M6);
	destroyArrayU(M7);
	destroyArrayU(T2);
	destroyArrayU(T3);
	destroyArrayU(T6);
}

void matMulTraditionalDouble(int n, double** A, double** B, double** C) {
	int i, j, k;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			C[i][j] = 0;
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
}

void matMulTraditionalFloat(int n, float** A, float** B, float** C) {
	int i, j, k;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			C[i][j] = 0;
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
}

void matMulTraditionalMPFR(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C) {
	int i, j, k;
	mpfr_t temp;
	mpfr_init2(temp, MPFR_PREC);

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			mpfr_set_d(C[i][j], 0.0, MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				mpfr_mul(temp, A[i][k], B[k][j], MPFR_ROUNDING_MODE);
				mpfr_add(C[i][j], C[i][j], temp, MPFR_ROUNDING_MODE);
			}
		}
	}

	mpfr_clear(temp);
}

void matMulTraditionalMPFRExact(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C) {
	int i, j, k;
	mpfr_t temp;
	mpfr_init2(temp, MPFR_PREC * 3);

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			mpfr_set_d(C[i][j], 0.0, MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < n; k++) {
				mpfr_mul(temp, A[i][k], B[k][j], MPFR_ROUNDING_MODE);
				mpfr_add(C[i][j], C[i][j], temp, MPFR_ROUNDING_MODE);
			}
		}
	}

	mpfr_clear(temp);
}

void matMulTraditionalMPFRFDP(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C) {
	int i, j, k;
	mpfr_t* vector1;
	mpfr_t* vector2;

	vector1 = malloc(n * sizeof(mpfr_t));
	vector2 = malloc(n * sizeof(mpfr_t));

	for (i = 0; i < n; ++i) {
		mpfr_init2(vector1[i], MPFR_PREC);
		mpfr_init2(vector2[i], MPFR_PREC);
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			mpfr_set_d(C[i][j], 0.0, MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {

			for (k = 0; k < n; k++) {
				mpfr_set(vector1[k], A[i][k], MPFR_ROUNDING_MODE);
				mpfr_set(vector2[k], B[k][j], MPFR_ROUNDING_MODE);
			}
			fusedDPMPFR(n, vector1, vector2, C[i][j]);
		}
	}

	free(vector1);
	free(vector2);
}

void matMulStrassenMPFR(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalMPFR(n, A, B, C);
		return;
	}

	int size = n >> 1;

	mpfr_t** A11 = createMPFRArray(size, size);
	mpfr_t** A12 = createMPFRArray(size, size);
	mpfr_t** A21 = createMPFRArray(size, size);
	mpfr_t** A22 = createMPFRArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A11[i][j], A[i][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A12[i][j], A[i][j + size], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A21[i][j], A[i + size][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A22[i][j], A[i + size][j + size], MPFR_ROUNDING_MODE);
		}
	}

	mpfr_t** B11 = createMPFRArray(size, size);
	mpfr_t** B12 = createMPFRArray(size, size);
	mpfr_t** B21 = createMPFRArray(size, size);
	mpfr_t** B22 = createMPFRArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B11[i][j], B[i][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B12[i][j], B[i][j + size], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B21[i][j], B[i + size][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B22[i][j], B[i + size][j + size], MPFR_ROUNDING_MODE);
		}
	}

	mpfr_t** S1 = createMPFRArray(size, size);
	mpfr_t** S2 = createMPFRArray(size, size);
	mpfr_t** S3 = createMPFRArray(size, size);
	mpfr_t** S4 = createMPFRArray(size, size);
	mpfr_t** S5 = createMPFRArray(size, size);
	mpfr_t** S6 = createMPFRArray(size, size);
	mpfr_t** S7 = createMPFRArray(size, size);
	mpfr_t** S8 = createMPFRArray(size, size);
	mpfr_t** M1 = createMPFRArray(size, size);
	mpfr_t** M2 = createMPFRArray(size, size);
	mpfr_t** M3 = createMPFRArray(size, size);
	mpfr_t** M4 = createMPFRArray(size, size);
	mpfr_t** M5 = createMPFRArray(size, size);
	mpfr_t** M6 = createMPFRArray(size, size);
	mpfr_t** M7 = createMPFRArray(size, size);
	mpfr_t** T2 = createMPFRArray(size, size);
	mpfr_t** T3 = createMPFRArray(size, size);
	mpfr_t** T6 = createMPFRArray(size, size);
	mpfr_t** C11 = createMPFRArray(size, size);
	mpfr_t** C12 = createMPFRArray(size, size);
	mpfr_t** C21 = createMPFRArray(size, size);
	mpfr_t** C22 = createMPFRArray(size, size);

	matrixAddMPFR(A21, A22, S1, size);
	matrixSubMPFR(S1, A11, S2, size);
	matrixSubMPFR(A11, A21, S3, size);
	matrixSubMPFR(A12, S2, S4, size);
	matrixSubMPFR(B12, B11, S5, size);
	matrixSubMPFR(B22, S5, S6, size);
	matrixSubMPFR(B22, B12, S7, size);
	matrixSubMPFR(B21, S6, S8, size);
	matMulStrassenMPFR(size, A11, B11, M1);
	matMulStrassenMPFR(size, A12, B21, M2);
	matMulStrassenMPFR(size, S1, S5, M3);
	matMulStrassenMPFR(size, S2, S6, M4);
	matMulStrassenMPFR(size, S3, S7, M5);
	matMulStrassenMPFR(size, S4, B22, M6);
	matMulStrassenMPFR(size, A22, S8, M7);
	matrixAddMPFR(M1, M2, C11, size);
	matrixAddMPFR(M1, M4, T2, size);
	matrixAddMPFR(T2, M5, T3, size);
	matrixAddMPFR(T3, M7, C21, size);
	matrixAddMPFR(T3, M3, C22, size);
	matrixAddMPFR(T2, M3, T6, size);
	matrixAddMPFR(T6, M6, C12, size);
	combineMatricesMPFR(C11, C12, C21, C22, n, C);

	destroyArrayMPFR(A11, size);
	destroyArrayMPFR(A12, size);
	destroyArrayMPFR(A21, size);
	destroyArrayMPFR(A22, size);
	destroyArrayMPFR(B11, size);
	destroyArrayMPFR(B12, size);
	destroyArrayMPFR(B21, size);
	destroyArrayMPFR(B22, size);
	destroyArrayMPFR(C11, size);
	destroyArrayMPFR(C12, size);
	destroyArrayMPFR(C21, size);
	destroyArrayMPFR(C22, size);
	destroyArrayMPFR(S1, size);
	destroyArrayMPFR(S2, size);
	destroyArrayMPFR(S3, size);
	destroyArrayMPFR(S4, size);
	destroyArrayMPFR(S5, size);
	destroyArrayMPFR(S6, size);
	destroyArrayMPFR(S7, size);
	destroyArrayMPFR(S8, size);
	destroyArrayMPFR(M1, size);
	destroyArrayMPFR(M2, size);
	destroyArrayMPFR(M3, size);
	destroyArrayMPFR(M4, size);
	destroyArrayMPFR(M5, size);
	destroyArrayMPFR(M6, size);
	destroyArrayMPFR(M7, size);
	destroyArrayMPFR(T2, size);
	destroyArrayMPFR(T3, size);
	destroyArrayMPFR(T6, size);
}

void matMulStrassenMPFRFusedDP(int n, mpfr_t** A, mpfr_t** B, mpfr_t** C) {
	int i, j;

	if (!IS_POWER_OF_2(n)) {
		printf("n should be power of 2!");
		return;
	}

	if (n == BLOCK) {
		matMulTraditionalMPFRFDP(n, A, B, C);
		return;
	}

	int size = n >> 1;

	mpfr_t** A11 = createMPFRArray(size, size);
	mpfr_t** A12 = createMPFRArray(size, size);
	mpfr_t** A21 = createMPFRArray(size, size);
	mpfr_t** A22 = createMPFRArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A11[i][j], A[i][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A12[i][j], A[i][j + size], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A21[i][j], A[i + size][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(A22[i][j], A[i + size][j + size], MPFR_ROUNDING_MODE);
		}
	}

	mpfr_t** B11 = createMPFRArray(size, size);
	mpfr_t** B12 = createMPFRArray(size, size);
	mpfr_t** B21 = createMPFRArray(size, size);
	mpfr_t** B22 = createMPFRArray(size, size);

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B11[i][j], B[i][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B12[i][j], B[i][j + size], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B21[i][j], B[i + size][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(B22[i][j], B[i + size][j + size], MPFR_ROUNDING_MODE);
		}
	}

	mpfr_t** S1 = createMPFRArray(size, size);
	mpfr_t** S2 = createMPFRArray(size, size);
	mpfr_t** S3 = createMPFRArray(size, size);
	mpfr_t** S4 = createMPFRArray(size, size);
	mpfr_t** S5 = createMPFRArray(size, size);
	mpfr_t** S6 = createMPFRArray(size, size);
	mpfr_t** S7 = createMPFRArray(size, size);
	mpfr_t** S8 = createMPFRArray(size, size);
	mpfr_t** M1 = createMPFRArray(size, size);
	mpfr_t** M2 = createMPFRArray(size, size);
	mpfr_t** M3 = createMPFRArray(size, size);
	mpfr_t** M4 = createMPFRArray(size, size);
	mpfr_t** M5 = createMPFRArray(size, size);
	mpfr_t** M6 = createMPFRArray(size, size);
	mpfr_t** M7 = createMPFRArray(size, size);
	mpfr_t** T2 = createMPFRArray(size, size);
	mpfr_t** T3 = createMPFRArray(size, size);
	mpfr_t** T6 = createMPFRArray(size, size);
	mpfr_t** C11 = createMPFRArray(size, size);
	mpfr_t** C12 = createMPFRArray(size, size);
	mpfr_t** C21 = createMPFRArray(size, size);
	mpfr_t** C22 = createMPFRArray(size, size);

	matrixAddMPFR(A21, A22, S1, size);
	matrixSubMPFR(S1, A11, S2, size);
	matrixSubMPFR(A11, A21, S3, size);
	matrixSubMPFR(A12, S2, S4, size);
	matrixSubMPFR(B12, B11, S5, size);
	matrixSubMPFR(B22, S5, S6, size);
	matrixSubMPFR(B22, B12, S7, size);
	matrixSubMPFR(B21, S6, S8, size);
	matMulStrassenMPFRFusedDP(size, A11, B11, M1);
	matMulStrassenMPFRFusedDP(size, A12, B21, M2);
	matMulStrassenMPFRFusedDP(size, S1, S5, M3);
	matMulStrassenMPFRFusedDP(size, S2, S6, M4);
	matMulStrassenMPFRFusedDP(size, S3, S7, M5);
	matMulStrassenMPFRFusedDP(size, S4, B22, M6);
	matMulStrassenMPFRFusedDP(size, A22, S8, M7);
	matrixAddMPFR(M1, M2, C11, size);
	matrixAddMPFR(M1, M4, T2, size);
	matrixAddMPFR(T2, M5, T3, size);
	matrixAddMPFR(T3, M7, C21, size);
	matrixAddMPFR(T3, M3, C22, size);
	matrixAddMPFR(T2, M3, T6, size);
	matrixAddMPFR(T6, M6, C12, size);
	combineMatricesMPFR(C11, C12, C21, C22, n, C);

	destroyArrayMPFR(A11, size);
	destroyArrayMPFR(A12, size);
	destroyArrayMPFR(A21, size);
	destroyArrayMPFR(A22, size);
	destroyArrayMPFR(B11, size);
	destroyArrayMPFR(B12, size);
	destroyArrayMPFR(B21, size);
	destroyArrayMPFR(B22, size);
	destroyArrayMPFR(C11, size);
	destroyArrayMPFR(C12, size);
	destroyArrayMPFR(C21, size);
	destroyArrayMPFR(C22, size);
	destroyArrayMPFR(S1, size);
	destroyArrayMPFR(S2, size);
	destroyArrayMPFR(S3, size);
	destroyArrayMPFR(S4, size);
	destroyArrayMPFR(S5, size);
	destroyArrayMPFR(S6, size);
	destroyArrayMPFR(S7, size);
	destroyArrayMPFR(S8, size);
	destroyArrayMPFR(M1, size);
	destroyArrayMPFR(M2, size);
	destroyArrayMPFR(M3, size);
	destroyArrayMPFR(M4, size);
	destroyArrayMPFR(M5, size);
	destroyArrayMPFR(M6, size);
	destroyArrayMPFR(M7, size);
	destroyArrayMPFR(T2, size);
	destroyArrayMPFR(T3, size);
	destroyArrayMPFR(T6, size);
}

void fusedDPMPFR(int n, mpfr_t* A, mpfr_t* B, mpfr_t result) {
	int i;
	mpfr_t temp_result, multiplied;

	//get the product and do the exact addition
	//(because the experiment values are between -1, 1 we're taking
	//a shortcut by assigning the precision to be 5 times MPFR_PREC)
	mpfr_init2(temp_result, MPFR_PREC * 8);
	mpfr_init2(multiplied, MPFR_PREC * 3);

	mpfr_set_d(temp_result, 0.0, MPFR_ROUNDING_MODE);

	for (i = 0; i < n; ++i) {
		mpfr_mul(multiplied, A[i], B[i], MPFR_ROUNDING_MODE);
		mpfr_add(temp_result, temp_result, multiplied, MPFR_ROUNDING_MODE);
	}

	//round and get result
	mpfr_set(result, temp_result, MPFR_ROUNDING_MODE);
	return;
}

float** createFloatArray(int rows, int columns) {
	float* values = calloc(rows * columns, sizeof(float));
	float** valueRows = malloc(rows * sizeof(float*));
	for (int i = 0; i < rows; ++i) {
		valueRows[i] = values + i * columns;
	}
	return valueRows;
}

double** createDoubleArray(int rows, int columns) {
	double* values = calloc(rows * columns, sizeof(double));
	double** valueRows = malloc(rows * sizeof(double*));
	for (int i = 0; i < rows; ++i) {
		valueRows[i] = values + i * columns;
	}
	return valueRows;
}

ubound_t** createUboundArray(int rows, int columns) {
	ubound_t* values = calloc(rows * columns, sizeof(ubound_t));
	ubound_t** valueRows = malloc(rows * sizeof(ubound_t*));
	for (int i = 0; i < rows; ++i) {
		valueRows[i] = values + i * columns;
	}
	return valueRows;
}

mpfr_t** createMPFRArray(int rows, int columns) {
	int size = rows * columns, i;
	mpfr_t *num_arr;
	num_arr = calloc(size, sizeof(mpfr_t));
	for (i = 0; i < size; i++) {
		mpfr_init2(num_arr[i], MPFR_PREC);
	}
	mpfr_t** valueRows = malloc(rows * sizeof(mpfr_t*));
	for (int i = 0; i < rows; ++i) {
		valueRows[i] = num_arr + i * columns;
	}
	return valueRows;
}

mpfr_t** createMPFRExactArray(int rows, int columns) {
	int size = rows * columns, i;
	mpfr_t *num_arr;
	num_arr = calloc(size, sizeof(mpfr_t));
	for (i = 0; i < size; i++) {
		mpfr_init2(num_arr[i], MPFR_PREC * 8);
	}
	mpfr_t** valueRows = malloc(rows * sizeof(mpfr_t*));
	for (int i = 0; i < rows; ++i) {
		valueRows[i] = num_arr + i * columns;
	}
	return valueRows;
}

void assignRandomDoubleMatrix(double** matrix, int rows, int columns) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; j++) {
			matrix[i][j] = -1 + 2 * ((double) rand()) / RAND_MAX;
		}
	}
	return;
}

void assignDoubleToFloatMatrix(double** matrixd, float** matrixf, int rows,
		int columns) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; ++j) {
			matrixf[i][j] = (float) matrixd[i][j];
		}
	}
}

void assignDoubleToUboundMatrix(double** matrixd, ubound_t** matrixU, int rows,
		int columns) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; ++j) {
			x2ub(matrixd[i][j], &matrixU[i][j]);
		}
	}
}

void assignMPFRToUboundMatrix(mpfr_t** matrixmpfr, ubound_t** matrixU, int rows,
		int columns) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; ++j) {
			x2ub(mpfr_get_d(matrixmpfr[i][j], MPFR_ROUNDING_MODE),
					&matrixU[i][j]);
		}
	}
}

void assignDoubleToMPFRMatrix(double** matrixd, mpfr_t** matrixMPFR, int rows,
		int columns) {
	int i, j;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < columns; j++) {
			mpfr_set_d(matrixMPFR[i][j], matrixd[i][j], MPFR_ROUNDING_MODE);
		}
	}
}

void matrixAddU(ubound_t** A, ubound_t** B, ubound_t** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			plusubound(&C[i][j], A[i][j], B[i][j]);
		}
	}
	return;
}

void matrixSubU(ubound_t** A, ubound_t** B, ubound_t** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			minusubound(&C[i][j], A[i][j], B[i][j]);
		}
	}
	return;
}

void combineMatricesU(ubound_t** C11, ubound_t** C12, ubound_t** C21,
		ubound_t** C22, int n, ubound_t** result) {
	int size = n / 2, i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			result[i][j] = C11[i][j];
			result[i][j + size] = C12[i][j];
			result[i + size][j] = C21[i][j];
			result[i + size][j + size] = C22[i][j];
		}
	}

	return;
}

void destroyArray(float** arr) {
	free(*arr);
	free(arr);
}

void destroyArrayD(double** arr) {
	free(*arr);
	free(arr);
}

void destroyArrayU(ubound_t** arr) {
	free(*arr);
	free(arr);
}

void destroyArrayMPFR(mpfr_t** arr, int size) {
	int i, j;
	for (i = 0; i < size; ++i) {
		for (j = 0; j < size; ++j) {
			mpfr_clear(arr[i][j]);
		}
	}
	free(*arr);
	free(arr);
}

double getUboundWidth(ubound_t ub) {
	gbound_t gb;
	double dleft, dright, result;

	get_gbound_from_ubound(&gb, &ub);

	dleft = u2f(gb.left_bound);
	dright = u2f(gb.right_bound);
	result = (log10(dright/dleft));

	return result;
}

void printMatrix(size_t n, float** mat) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%.57f\t", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void printMatrixDouble(size_t n, double** mat) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%.57f\t", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void printErrorMatrix(int rows, int columns, double** mat) {
	int i, j;
	for (i = 0; i < rows; ++i) {
		for (j = 0; j < columns; ++j) {
			printf("%e ", mat[i][j]);
		}
		printf("\n");
	}
}

void printMatrixMPFR(size_t n, mpfr_t** mat) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%.57f\t", mpfr_get_d(mat[i][j], MPFR_ROUNDING_MODE));
		}
		printf("\n");
	}
	printf("\n");
}

void printWidthMatrixU(size_t n, ubound_t** mat, char* mat_name) {
	int i, j;
	printf("%s\tsize = %lu\n", mat_name, n);
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%e\t", getUboundWidth(mat[i][j]));
		}
		printf("\n");
	}
	printf("\n");
}

void printMatrixU(size_t n, ubound_t** mat, char* mat_name) {
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			printf("%s[%d][%d]=\n", mat_name, i, j);
			uboundview(mat[i][j]);
		}
	}
}

void printToFile(double **error, char* file_name) {
	char buffer[100];
	FILE *f;
	int i, j, k;
	double max, min, sum, sum_dev, mean;

	snprintf(buffer, sizeof(char) * 100, "%dx%d/%s", MAT_SIZE,
	MAT_SIZE, file_name);
	f = fopen(buffer, "w");
	fprintf(f, "\n%s\n", file_name);
	for (j = 0; j < MAT_SIZE; j++) {
		//fprintf(f, "{");
		for (k = 0; k < MAT_SIZE; k++) {
			max = 0.0, min = 1.0;
			sum_dev = 0.0;
			sum = 0.0;
			for (i = 0; i < REPEAT; i++) {
				sum += fabs(error[i][(j * MAT_SIZE) + k]);
				if (fabs(error[i][(j * MAT_SIZE) + k]) < fabs(min))
					min = error[i][(j * MAT_SIZE) + k];

				if (fabs(error[i][(j * MAT_SIZE) + k]) > fabs(max))
					max = error[i][(j * MAT_SIZE) + k];
			}
			mean = sum / REPEAT;
			for (i = 0; i < REPEAT; ++i)
				sum_dev += (fabs(error[i][(j * MAT_SIZE) + k]) - mean)
						* (fabs(error[i][(j * MAT_SIZE) + k]) - mean);
			/*fprintf(f,
			 "%d %d min = %e, max = %e, mean = %e, standard deviation = %e\n",
			 j, k, min, max, mean, sum_dev);*/
			/*if (k == (mat_size - 1))
			 fprintf(f, "%e", mean);
			 else*/
			fprintf(f, "%e, ", mean);
		}
		//fprintf(f, "}, ");
	}
	fclose(f);
}

/*snprintf(buffer, sizeof(char) * 100, "%dx%d/trad_mpfr.txt", mat_size,
 mat_size);
 f = fopen(buffer, "w");
 fprintf(f, "\nTraditional MPFR\n");
 for (j = 0; j < mat_size; j++) {
 //fprintf(f, "{");
 for (k = 0; k < mat_size; k++) {
 max = 0.0, min = 1.0;
 sum_dev = 0.0;
 sum = 0.0;
 for (i = 0; i < REPEAT; i++) {
 sum += fabs(eTradMPFR[i][(j * mat_size) + k]);
 if (fabs(eTradMPFR[i][(j * mat_size) + k]) < fabs(min))
 min = eTradMPFR[i][(j * mat_size) + k];

 if (fabs(eTradMPFR[i][(j * mat_size) + k]) > fabs(max))
 max = eTradMPFR[i][(j * mat_size) + k];
 }
 mean = sum / REPEAT;
 for (i = 0; i < REPEAT; ++i)
 sum_dev += (fabs(eTradMPFR[i][(j * mat_size) + k]) - mean)
 * (fabs(eTradMPFR[i][(j * mat_size) + k]) - mean);
 fprintf(f,
 "%d %d min = %e, max = %e, mean = %e, standard deviation = %e\n",
 j, k, min, max, mean, sum_dev);
 if (k == (mat_size - 1))
 fprintf(f, "%e", mean);
 else
 fprintf(f, "%e, ", mean);
 }
 //fprintf(f, "}, ");
 }
 fclose(f);

 snprintf(buffer, sizeof(char) * 100,
 "%dx%d/stats_Strassen_unum_fused_block.txt", mat_size, mat_size);
 f = fopen(buffer, "w");
 fprintf(f, "\nStrassen Unum\n");
 for (j = 0; j < mat_size; j++) {
 //fprintf(f, "{");
 for (k = 0; k < mat_size; k++) {
 max = 0.0, min = 1.0;
 sum_dev = 0.0;
 sum = 0.0;
 for (i = 0; i < REPEAT; i++) {
 sum += fabs(eStrassenUnum[i][(j * mat_size) + k]);
 if (fabs(eStrassenUnum[i][(j * mat_size) + k]) < fabs(min))
 min = eStrassenUnum[i][(j * mat_size) + k];

 if (fabs(eStrassenUnum[i][(j * mat_size) + k]) > fabs(max))
 max = eStrassenUnum[i][(j * mat_size) + k];
 }
 mean = sum / REPEAT;
 for (i = 0; i < REPEAT; ++i)
 sum_dev += (fabs(eStrassenUnum[i][(j * mat_size) + k]) - mean)
 * (fabs(eStrassenUnum[i][(j * mat_size) + k]) - mean);
 fprintf(f,
 "%d %d min = %e, max = %e, mean = %e, standard deviation = %e\n",
 j, k, min, max, mean, sum_dev);
 if (k == (mat_size - 1))
 fprintf(f, "%e", mean);
 else
 fprintf(f, "%e, ", mean);
 }
 //fprintf(f, "}, ");
 }
 fclose(f);*/

void matrixAddMPFR(mpfr_t** A, mpfr_t** B, mpfr_t** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_add(C[i][j], A[i][j], B[i][j], MPFR_ROUNDING_MODE);
		}
	}
	return;
}

void matrixSubMPFR(mpfr_t** A, mpfr_t** B, mpfr_t** C, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_sub(C[i][j], A[i][j], B[i][j], MPFR_ROUNDING_MODE);
		}
	}
	return;
}

void combineMatricesMPFR(mpfr_t** C11, mpfr_t** C12, mpfr_t** C21, mpfr_t** C22,
		int n, mpfr_t** result) {
	int size = n / 2, i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(result[i][j], C11[i][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(result[i][j + size], C12[i][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(result[i + size][j], C21[i][j], MPFR_ROUNDING_MODE);
		}
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			mpfr_set(result[i + size][j + size], C22[i][j], MPFR_ROUNDING_MODE);
		}
	}
	return;
}
